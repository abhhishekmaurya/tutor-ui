import { Component, OnInit, TemplateRef } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { Router } from '@angular/router';
// import {Observable} from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

@Component({
  selector: 'app-viewagent',
  templateUrl: './viewagent.component.html',
  styleUrls: ['./viewagent.component.css']
})
export class ViewagentComponent implements OnInit {
  [x: string]: any;
  _countrys: any;
  fl1: boolean;
  city1: any;
  coinsList: any;
  public temp_var: Object = false;
  bsModalRef: BsModalRef;
  user: string[];
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  alerts1: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  dismissible = true;
  public ageants1;
  public food1;
  modalRef: BsModalRef;

  public firstname;
  public lastname;
  public EmailID;
  public subject;
  public Company;
  public Address;
  public Location;
  public Country;
  public Status;

  agentsUser = { "meta": { "code": 200, "message": "Records found" }, "data": [{ "id": 1, "firstName": "Mithun", "lastName": "Mondal", "address": "String", "email": "String", "country": "String", "company": "String", "status": "String", "createdAt": "28-03-2018 09:56:27 AM", "updatedAt": "05-04-2018 01:09:47 PM", "activeFl": true }, { "id": 2, "firstName": "Mithun", "lastName": "Mondal", "address": "String", "email": "String", "country": "String", "company": "Software", "status": "String", "createdAt": "05-04-2018 12:30:27 PM", "updatedAt": "05-04-2018 01:08:39 PM", "activeFl": false }] };
  dtOptions: any = {};
  constructor(private _demoService: MyserviceService, private modalService: BsModalService, private router: Router) {
    if (!this.coinsList) {
      this.getAgents();
    }
  }

  cleardata() {
    this.firstname = "";
    this.lastname = "";
    this.EmailID = "";
    this.subject = "";
    this.Company = "";
    this.Address = "";
    this.Location = "";
    this.Country = "";
    this.Status = "";


  }
  ngOnInit() {
    this.dtOptions = {

      paging: true,
      pageLength: 10,
      processing: true,
      columnDefs: [ {

        targets: [0,6], // column or columns numbers
        
        orderable: false,  // set orderable for selected columns
        
        }]

    };

    this.counrty = "sel";

    this.getCountry();
    localStorage.setItem('agentsUser1', JSON.stringify(this.agentsUser.data));

  }
  ngOnDestroy() {
    if (sessionStorage.models == 'true') {
      this.modalRef.hide();
      sessionStorage.models = false;
    }

  }

  username = this._demoService.username;
  openModal(template: TemplateRef<any>, f) {
    sessionStorage.models = 'true';
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.food1 = f;
    this.food1.firstName = f.firstName;
    this.modalRef = this.modalService.show(template);

  }

  openModal1(template: TemplateRef<any>) {
    sessionStorage.models = 'true';
    this.cleardata();
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.modalRef = this.modalService.show(template);

  }

  openModalWithComponent(template: TemplateRef<any>, f) {
    sessionStorage.models = 'true';
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.food1 = f;
    this.food1.firstName = f.firstName;
    this.modalRef = this.modalService.show(template);
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  getAgents() {

    let url = '/plm/trainers?filter=%7B%7D';
    this._demoService.getfcountry(url).subscribe(
      data => {
        this.ageants1 = data;
        this.ageants1 = this.ageants1.data;
        this.coinsList = this.ageants1;
        this.temp_var = true;

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));
        return Observable.throw(error);
      },

    )

  }


  checkAll(ev) {

    this.coinsList.forEach(x => x.state = ev.target.checked);

  }

  isAllChecked() {

    return this.coinsList.every(_ => _.state);
  }

  deleteAgents(x) {

    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];

    if (confirm("Are you sure you want to delete " + "?")) {
      let url = '/plm/trainer?ids=' + x.id;
      this._demoService.delete(url).subscribe(
        data => {
          this.ageants1 = data;
          this.alerts.push({
            type: 'success',
            msg: this.ageants1.data[0],
            timeout: 5000
          });
          this.getAgents();

        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.error.meta.error_message,
            timeout: 5000
          });

          return Observable.throw(error);
        }


      )


    }

  }
  editAgents(u) {
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];

    if (!u.firstName) {

      this.alerts1.push({
        type: 'danger',
        msg: `firstName is required !!!`,
        timeout: 5000
      });
    }
    else if (!u.lastName) {

      this.alerts1.push({
        type: 'danger',
        msg: `lastName is required !!!`,
        timeout: 5000
      });
    }
    else if (!u.email) {

      this.alerts1.push({
        type: 'danger',
        msg: `EmailId is required !!!`,
        timeout: 5000
      });
    }
    else if (!u.company) {

      this.alerts1.push({
        type: 'danger',
        msg: `Compnay name is required !!!`,
        timeout: 5000
      });
    }
    else if (!u.address) {

      this.alerts1.push({
        type: 'danger',
        msg: `Address is required !!!`,
        timeout: 5000
      });
    }
    else if (!u.country) {

      this.alerts1.push({
        type: 'danger',
        msg: `Country is required !!!`,
        timeout: 5000
      });
    }
    else if (!u.status) {

      this.alerts1.push({
        type: 'danger',
        msg: `Status is required !!!`,
        timeout: 5000
      });
    }
    else {
      let url = '/plm/trainer';
      this._demoService.edits(url, u).subscribe(
        data => {
          this.ageants1 = data;
          this.getAgents();

          this.alerts.push({
            type: 'success',
            msg: `Record Updated Sucessfully !!!`,
            timeout: 5000
          });

        },
        error => {


          this.alerts.push({
            type: 'danger',
            msg: error.error.meta.error_message,
            timeout: 5000
          });
          return Observable.throw(error);
        }



      )

      this.modalRef.hide();

    }


  }

  addnewagent(firstname, lastname, EmailID, subject, Company, Address, Location, Country) {


    if (!firstname) {

      this.alerts1.push({
        type: 'danger',
        msg: `firstname is required !!!`,
        timeout: 5000
      });
    }

    else if (!lastname) {

      this.alerts1.push({
        type: 'danger',
        msg: `lastname is required !!!`,
        timeout: 5000
      });
    }
    else if (!EmailID) {

      this.alerts1.push({
        type: 'danger',
        msg: `EmailID is required !!!`,
        timeout: 5000
      });
    }
    else if (!subject) {

      this.alerts1.push({
        type: 'danger',
        msg: `Subject name is required !!!`,
        timeout: 5000
      });
    }
    else if (!Company) {

      this.alerts1.push({
        type: 'danger',
        msg: `Compnay name is required !!!`,
        timeout: 5000
      });
    }
    else if (!Address) {

      this.alerts1.push({
        type: 'danger',
        msg: `Address name is required !!!`,
        timeout: 5000
      });
    }
    else if (!Location) {

      this.alerts1.push({
        type: 'danger',
        msg: `City  is required !!!`,
        timeout: 5000
      });
    }
    else if (!Country) {

      this.alerts1.push({
        type: 'danger',
        msg: `Country is required !!!`,
        timeout: 5000
      });
    }

    else {
      this.counrty = parseInt(this.counrty);
      this.cityids = parseInt(this.cityids);
      var userid = parseInt(sessionStorage.uid);

      let addnewagent = {
        "address": Address,
        "email": EmailID,
        "firstName": firstname,
        "lastName": lastname,
        "company": Company,
        "plmUser": {
          "id": userid
        },
        "country": {
          "id": this.counrty
        },
        "city": {
          "id": this.cityids
        }
      };
      let url = "/plm/trainer?userId=";
      this._demoService.postdata(url, addnewagent).subscribe(
        data => {
          this.ageants1 = data;
          this.getAgents();
          this.alerts.push({
            type: 'success',
            msg: `Trainer Added Sucessfully!`,
            timeout: 5000
          });

        },
        error => {
          alert(JSON.stringify(error.error.meta.error_message));
          this.alerts.push({
            type: 'danger',
            msg: error,
            timeout: 5000
          });

        }

      )
      this.modalRef.hide();
    }

  }

  changeCity() {

    if (this.counrty == 'USA') {

      this.city = ["Newyork", "Sandiago", "Losangel"];

    }
    else if (this.counrty == 'China') {
      this.city = ["Beging", "Shanghai", "Guangzhou", "Hefei (May 23 - May 25)", "Hefei (May 28 - May 30)"];
    }
    this.getcitybycountry(this.counrty);

  }

  getCountry() {
    this.fl1 = true;
    this._demoService.gettutorcountry().subscribe(
      data => {
        this._countrys = data;
      },
      error => {
        alert("Server Error!!!");
        this.fl1 = false;
      },

    )
  }

  getcitybycountry(cid) {

    this._demoService.getcitybycid(cid).subscribe(
      data => {
        this.city1 = data;

        this.fl1 = false;

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));
        this.fl1 = false;
      },

    )
  }
}





