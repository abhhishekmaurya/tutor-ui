import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewagentComponent } from './viewagent.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule, ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { HeadervComponent } from '../headerv/headerv.component';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';

describe('ViewagentComponent', () => {
  let component: ViewagentComponent;
  let fixture: ComponentFixture<ViewagentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientModule,AlertModule.forRoot(),FormsModule,ModalModule.forRoot(),BsDatepickerModule.forRoot(),ReactiveFormsModule,DataTablesModule],
      declarations: [ ViewagentComponent,HeadervComponent ],
      providers : [MyserviceService,AuthGuard]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewagentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
