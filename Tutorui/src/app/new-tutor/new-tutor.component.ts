import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import { MyserviceService } from '../services/myservice.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HeadervComponent } from '../headerv/headerv.component';
import { DISABLED } from '@angular/forms/src/model';
import { AlertComponent } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

declare var $: any;
@Component({
  selector: 'app-new-tutor',
  templateUrl: './new-tutor.component.html',
  styleUrls: ['./new-tutor.component.css']
})
export class NewTutorComponent implements OnInit {
  _countrys1: Object;
  cityds = [];
  counrty1 = [];
  addressgg: string;
  Trainerss1 = [];
  public cityids: any;
  public Trainerss: any;
  trainer: any;
  private daf;
  private _countrys: any;
  fl1: boolean;
  counrty: any;
  public city1: any;
  public cityf1: any;
  public city;
  secity: any;
  scountry: any;
  selectedValuedd: any;
  tutorTE: any;
  tutorTOE: any;
  tutorTASE: any;
  tutorTAE: any;
  tutorTTA: any;
  tutorTPAS: any;
  data1: any;
  fl: boolean;
  f1: any;
  isDisabled51: string;
  isDisabled55: string;
  isDisabled5: string;
  isDisabled4: string;
  isDisabled3: any;
  tuorff: { "meta": { "code": number; "message": string; }; "data": { "id": number; "fullName": string; "residingAddress": string; "dob": string; "highestDegree": string; "majorMinor": string; "institutionOfDegree": string; "yearOfDegreeReceived": string; "registeringLocation": string; "tutorTPA": any[]; "tutorTEA": any[]; "tutorTPN": any[]; "tutorTPAS": any[]; "tutorTTA": any[]; "tutorTAE": any[]; "tutorTASE": any[]; "tutorTOE": any[]; "tutorTE": any[]; "tutorTCS": any[]; "tutorTKL": any[]; "tutorTA": any[]; "tutorTrainers": any[]; "tutorSubjects": any[]; "tutorOrders": any[]; "createdAt": string; "updatedAt": string; "activeFl": boolean; }[]; };
  tuorff1: any;
  country = ["USA", "China"];
  isDisabled1: any;
  isDisabled: any;
  selectedValue1 = [];

  public tutor: any = {

  };
  selectedCarId = 'Shanghai';

  public next = 1;
  public subject1: any = [];
  public aceept: any = [];
  public invoiceForm: FormGroup;
  datePipe = new DatePipe("en-US");
  selectedValue = [];
  public Fullname;
  public dob;
  public highestDegree;
  public majorMinor;
  public institutionOfDegree;
  public Address;
  public yearOfDegreeReceived;
  public registeringLocation;
  public Status;
  public previousaddress;
  public EmailAddress1;
  public previousaddress1;
  public tutor1;
  public bts = true;
  public dat;
  modalRef: BsModalRef;
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  dismissible = true;
  constructor(private _fb: FormBuilder, private _demoService: MyserviceService, private modalService: BsModalService, private router: Router) {

    if (sessionStorage.fname || sessionStorage.lname) {

      this.tutor.remail = sessionStorage.emails;
      this.tutor.Firstname = sessionStorage.fname;
      this.tutor.Lastname = sessionStorage.lname;

    }

    this.dat = new Date();

    this.counrty = "sel";
    this.cityids = "sel";

    this.Trainerss = "sel";
    this.getCountry();
    this.getFutureCountry();
    this.getcitybycountry(1);

  }



  ngOnInit() {

    this.tutor.fcounrty = "sel";
    this.tutor.cityidss = "sel";
    this.invoiceForm = this._fb.group({
      tutorTPA: this._fb.array([this.initItemRows()]),
      tutorTEA: this._fb.array([this.initItemRows1()]),
      tutorTPN: this._fb.array([this.initItemRows2()]),
      tutorTPAS: this._fb.array([this.initItemRows3()]),
      tutorTTA: this._fb.array([this.initItemRows4()]),
      tutorTAE: this._fb.array([this.initItemRows5()]),
      tutorTASE: this._fb.array([this.initItemRows6()]),
      tutorTOE: this._fb.array([this.initItemRows7()]),
      tutorTE: this._fb.array([this.initItemRows8()])
    })



  }

  openModal(template: TemplateRef<any>, f) {

    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];

    this.f1 = f;
    this.modalRef = this.modalService.show(template);
    this.addNewTutor(f);

  }
  initItemRows() {
    return this._fb.group({
      address: ['']
    });
  }
  initItemRows1() {
    return this._fb.group({
      "email": ['', Validators.pattern],
      "typeOfAddress": ['1']

    });
  }
  initItemRows2() {
    return this._fb.group({
      "phoneNumber": ['',Validators.pattern],
      "typeOfNumber": ['1']

    });
  }

  initItemRows3() {
    return this._fb.group({
      "name": [''],
      "date": [''],
      "duration": ['']


    });
  }
  initItemRows4() {
    return this._fb.group({
      "date": [''],
      "location": [''],
      "accountNumber": ['']


    });


  }
  initItemRows5() {
    return this._fb.group({
      "date": [''],
      "location": ['']


    });


  }
  initItemRows6() {
    return this._fb.group({
      "name": [''],
      "url": ['']


    });
  }
  initItemRows7() {
    return this._fb.group({
      "name": [''],
      "url": ['']


    });
  }
  initItemRows8() {
    return this._fb.group({
      "name": [''],
      "locationOfSchool": [''],
      "gradeLvlOfStudent": [''],
      "subjectTaught": ['']


    });


  }

  username = this._demoService.username;
  /********************************* */
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
  /************************************************ */
  addtutorTPA() {

    const control = <FormArray>this.invoiceForm.controls['tutorTPA'];
    control.push(this.initItemRows());

  }
  removetutorTpa(index: number) {

    const control = <FormArray>this.invoiceForm.controls['tutorTPA'];
    control.removeAt(index);

  }

  /******************************************************* */
  addtutorTPN() {
    const control = <FormArray>this.invoiceForm.controls['tutorTPN'];
    control.push(this.initItemRows2());

  }
  removetutorTPN(index: number) {

    const control = <FormArray>this.invoiceForm.controls['tutorTPN'];
    control.removeAt(index);

  }
  /***************************************************************************** */
  addtutorTEA() {
    const control = <FormArray>this.invoiceForm.controls['tutorTEA'];
    control.push(this.initItemRows1());

  }
  removetutorTea(index: number) {

    const control = <FormArray>this.invoiceForm.controls['tutorTEA'];
    control.removeAt(index);

  }
  /******************************************************************** */
  addtutorTPAS() {
    const control = <FormArray>this.invoiceForm.controls['tutorTPAS'];
    control.push(this.initItemRows3());

  }
  removetutorTPAS(index: number) {

    const control = <FormArray>this.invoiceForm.controls['tutorTPAS'];
    control.removeAt(index);

  }
  /**************************************************************************************** */
  addtutorTTA() {
    const control = <FormArray>this.invoiceForm.controls['tutorTTA'];
    control.push(this.initItemRows4());

  }
  removetutorTTA(index: number) {

    const control = <FormArray>this.invoiceForm.controls['tutorTTA'];
    control.removeAt(index);

  }
  /****************************************************************************************** */
  addtutorTAE() {

    const control = <FormArray>this.invoiceForm.controls['tutorTAE'];
    control.push(this.initItemRows5());
  }
  removetutorTAE(index: number) {

    const control = <FormArray>this.invoiceForm.controls['tutorTAE'];
    control.removeAt(index);

  }
  /****************************************************************************************** */
  addtutorTASE() {
    const control = <FormArray>this.invoiceForm.controls['tutorTASE'];
    control.push(this.initItemRows6());
  }
  removetutorTASE(index: number) {
    const control = <FormArray>this.invoiceForm.controls['tutorTASE'];
    control.removeAt(index);
  }
  /******************************************************************************* */
  addtutorTOE() {
    const control = <FormArray>this.invoiceForm.controls['tutorTOE'];
    control.push(this.initItemRows7());
  }
  removetutorTOE(index: number) {
    const control = <FormArray>this.invoiceForm.controls['tutorTOE'];
    control.removeAt(index);
  }
  /***************************************************** */
  addtutorTE() {
    const control = <FormArray>this.invoiceForm.controls['tutorTE'];
    control.push(this.initItemRows8());
  }
  removetutorTE(index: number) {
    const control = <FormArray>this.invoiceForm.controls['tutorTE'];
    control.removeAt(index);
  }
  /************************************************************ */
  onSelectionChange() {

    this.invoiceForm.get('tutorTPAS').disable();



  }
  onSelectionChange1() {

    this.invoiceForm.get('tutorTPAS').enable();

  }

  onSelectTutotTa() {

    if (this.isDisabled3 == 'false') {
      this.invoiceForm.get('tutorTTA').enable();
    }
    if (this.isDisabled3 == 'true') {
      this.invoiceForm.get('tutorTTA').disable();
    }
  }
  onSelecttutorTAE(s1) {
    if (s1 == false) {
      this.invoiceForm.get('tutorTAE').enable();
    }
    if (s1 == true) {
      this.invoiceForm.get('tutorTAE').disable();
    }
  }
  onSelecttutorTASE(d3) {

    if (d3 == false) {
      this.invoiceForm.get('tutorTASE').enable();
    }
    if (d3 == true) {
      this.invoiceForm.get('tutorTASE').disable();
    }
  }
  onSelecttutorTOE(d4) {

    if (d4 == false) {
      this.invoiceForm.get('tutorTOE').enable();
    }
    if (d4 == true) {
      this.invoiceForm.get('tutorTOE').disable();
    }
  }
  onSelecttutorTE(se4) {
    if (se4 == false) {
      this.invoiceForm.get('tutorTE').enable();
    }
    if (se4 == true) {
      this.invoiceForm.get('tutorTE').disable();
    }
  }


  visut() {
    window.location.href = "http://www.act.org/content/act/en/products-and-services/beta/act-tutor-certification.html";
  }
  /******************************************************* */
  change(e, type) {

    if (type != 'All_Section_Of_ACT') {
      this.tutor.Allsectionofact = "";

    }



    if (type == 'All_Section_Of_ACT') {
      this.tutor.english = "tutor.english";
      this.tutor.Reading = "tutor.Reading";
      this.tutor.Math = "tutor.Math";
      this.tutor.Science = "tutor.Science";
      this.tutor.Writing = "tutor.Writing";
      this.tutor.Basics = "tutor.Basics";
      this.selectedValue.push('English');
      this.selectedValue.push('Reading');
      this.selectedValue.push('Math');
      this.selectedValue.push('Science');
      this.selectedValue.push('Writing');
      this.selectedValue.push('Basics');


    }

    if (e) {


      if (type != 'All_Section_Of_ACT') {

        this.selectedValue.push(type);
      }



    }
    else {

      let index = this.selectedValue.indexOf(type);

      this.selectedValue.splice(index, 1);

      if (type == 'All_Section_Of_ACT') {

          // alert("Empty all values "+this.selectedValue.length);
        this.tutor.english = "";
        this.tutor.Reading ="";
        this.tutor.Math = "";
        this.tutor.Science = "";
        this.tutor.Writing = "";
        this.tutor.Basics = "";
        this.selectedValue = [];

      }


    }
    if (this.selectedValue.length > 0) {

      this.selectedValue = Array.from(new Set(this.selectedValue));

    }
    if (this.tutor.english) {

      if (this.tutor.Reading)
        if (this.tutor.Math) {
          if (this.tutor.Science)
            if (this.tutor.Writing) {
              if (this.tutor.Basics)

                this.tutor.Allsectionofact = "tutor.Allsectionofact";
            }
        }
    }


  }
  /********************************************************************************************** */

  change1(e, type) {

    if (e) {
      this.selectedValue1.push(type);
    }
    else {

      let index = this.selectedValue1.indexOf(type);

      this.selectedValue1.splice(index, 1);

    }

  }
  /***************************************************************** */

  nextT() {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];

    if (!this.tutor.Firstname || !this.tutor.Lastname && !this.tutor.Residing) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (!this.tutor.dob) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 2 && !this.isDisabled) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 2 && this.isDisabled == "false" && !this.invoiceForm.value.tutorTPAS[0].duration) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill  name, duration and date  !!!`,
        timeout: 6000
      });

    }
    else if (this.next == 2 && this.isDisabled == "false" && (!this.invoiceForm.value.tutorTPAS[0].name || !this.invoiceForm.value.tutorTPAS[0].date)) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  name , duration and date  !!!`,
        timeout: 6000
      });
    }

    else if (this.next == 3 && !this.isDisabled3) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 3 && this.isDisabled3 == "false" && (!this.invoiceForm.value.tutorTTA[0].location || !this.invoiceForm.value.tutorTTA[0].date)) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill  location and date  !!!`,
        timeout: 6000
      });


    }
    else if (this.next == 4 && !this.isDisabled4) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 4 && this.isDisabled4 == "false" && (!this.invoiceForm.value.tutorTAE[0].location)) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill date and location also !!!`,
        timeout: 6000
      });


    }
    else if (this.next == 4 && this.isDisabled4 == "false" && (!this.invoiceForm.value.tutorTAE[0].date)) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill date and location also !!!`,
        timeout: 6000
      });


    }
    else if (this.next == 5 && !this.isDisabled5) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 5 && this.isDisabled5 == "false" && (!this.invoiceForm.value.tutorTASE[0].url || !this.invoiceForm.value.tutorTASE[0].name)) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill name and url also !!!`,
        timeout: 6000
      });


    }
    else if (this.next == 6 && !this.isDisabled55) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 6 && this.isDisabled55 == "false" && (!this.invoiceForm.value.tutorTOE[0].url)) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill name and url also !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 6 && this.isDisabled55 == "false" && (!this.invoiceForm.value.tutorTOE[0].name)) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill name and url also !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 7 && !this.isDisabled51) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 7 && this.isDisabled51 == "false" && (!this.invoiceForm.value.tutorTE[0].name || !this.invoiceForm.value.tutorTE[0].subjectTaught || !this.invoiceForm.value.tutorTE[0].locationOfSchool || !this.invoiceForm.value.tutorTE[0].gradeLvlOfStudent)) {

      this.alerts.push({
        type: 'danger',
        msg: `Please fill name, location, greade level, Subject(s) Taught  !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 8 && (!this.tutor.highestDegree || !this.tutor.majorMinor)) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 8 && (!this.tutor.instname || !this.tutor.yearOfDegreeReceived)) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 8 && this.selectedValue.length == 0) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 9 && !this.tutor.Verbal && !this.tutor.Speaking && !this.tutor.Reading1 && !this.tutor.Writing1) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill Verbal, Speaking, Reading, Writing field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 9 && !this.tutor.Reading1 && !this.tutor.Writing1) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill Verbal, Speaking, Reading, Writing field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 9 && !this.tutor.Writing1) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill Verbal, Speaking, Reading, Writing field !!!`,
        timeout: 6000
      });
    }
    else if (this.next == 9 && !this.tutor.loc) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill * required field !!!`,
        timeout: 6000
      });
    }
    else {
      this.bts = false;
      if (this.next < 12)
        this.next = this.next + 1;
      this.chk();
    }
  }
  changeCity() {

    this.cityids = "sel";
    this.Trainerss = "sel";
    this.addressgg = " ";
    this.tutor.loc = "";
    this.chk();
    if (this.counrty == 'USA') {

      this.city = ["Newyork", "Sandiago", "Losangel"];

    }
    else if (this.counrty == 'China') {
      this.city = ["Beging", "Shanghai", "Guangzhou", "Hefei (May 23 - May 25)", "Hefei (May 28 - May 30)"];
    }
    this.counrty1 = [];
    for (let t in this._countrys.data) {

      if (this.counrty == this._countrys.data[t].id) {

        this.counrty1.push(this._countrys.data[t]);
      }

    }

    if (this.counrty != "sel" && this.counrty) {
      this.getcitybycountry(this.counrty);

    }

  }
  changecity1() {
    if (this.tutor.fcounrty != "sel" && this.tutor.fcounrty) {

      this.getcitybyFuturecountry(this.tutor.fcounrty);
    }
  }
  nextT6() {

    this.tutor.loc = this.cityds[0].cityName + '  ' + this.counrty1[0].countryName;
    this.chk();
    this.Trainerss1.length = 0;

    this.addressgg = " ";

    if (this.Trainerss == '0') {

      this.addressgg = " ";
    }
    for (let t in this.trainer.data) {

      if (this.Trainerss == this.trainer.data[t].id) {

        this.Trainerss1.push(this.trainer.data[t]);

      }
    }


    this.addressgg = this.Trainerss1[0].address + ', ' + this.Trainerss1[0].city.cityName + ', ' + this.Trainerss1[0].country.countryName;

  }



  nextT5() {
    this.addressgg = " ";
    this.Trainerss = " ";
    this.tutor.loc = "";
    this.chk();
    var cid = this.counrty;
    var cityd = this.cityids;
    this.cityds = [];
    for (let t in this.city1.data) {
      if (cityd == this.city1.data[t].id) {

        this.cityds.push(this.city1.data[t]);
      }

    }

    if (cid && cityd) {
      this.gettrinerbyCity(cid, cityd);
      this.nextT2();
    }

  }
  getcity() {

    this.city = sessionStorage.citys;
  }
  next3() {
    this.data1 = this.invoiceForm.get('tutorTTA').value;

    if (this.next == 3 && !this.isDisabled3) {
      this.bts = true;
    }

    else if (this.next == 3 && (this.isDisabled3 == "false" && (!this.data1[0].location || !this.data1[0].date))) {

      this.bts = true;
      this.fl = true;


    }
    else if (this.next == 3 && this.isDisabled3 == "true") {

      this.bts = false;
      this.fl = false;
    }
    this.chk();

  }

  nextT2() {

    this.tutorTPAS = this.invoiceForm.get('tutorTPAS').value;
    this.tutorTTA = this.invoiceForm.get('tutorTTA').value;
    this.tutorTAE = this.invoiceForm.get('tutorTAE').value;
    this.tutorTASE = this.invoiceForm.get('tutorTASE').value;
    this.tutorTOE = this.invoiceForm.get('tutorTOE').value;
    this.tutorTE = this.invoiceForm.get('tutorTE').value;
    if (!this.tutor.Firstname || !this.tutor.Lastname || !this.tutor.Residing) {
      this.bts = true;

    }
    else if (!this.tutor.dob || !this.tutor.Residing) {
      this.bts = true;

    }
    else if (this.next == 2 && !this.isDisabled) {
      this.bts = true;


    }

    else if (this.next == 2 && this.isDisabled == "false" && (!this.tutorTPAS[0].name || !this.tutorTPAS[0].date)) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 2 && (this.isDisabled == "false" && (!this.tutorTPAS[0].duration || this.tutorTPAS[0].duration == undefined))) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 3 && !this.isDisabled3) {
      this.bts = true;
    }

    else if (this.next == 3 && this.isDisabled3 == "false" && (!this.tutorTTA[0].date || !this.tutorTTA[0].location)) {

      this.bts = true;
      this.fl = true;


    }
    else if (this.next == 4 && !this.isDisabled4) {
      this.bts = true;

    }
    else if (this.next == 4 && this.isDisabled4 == "false" && (!this.tutorTAE[0].location)) {
      this.bts = true;
      this.fl = true;


    }
    else if (this.next == 4 && this.isDisabled4 == "false" && (!this.tutorTAE[0].date)) {
      this.bts = true;
      this.fl = true;


    }
    else if (this.next == 5 && !this.isDisabled5) {
      this.bts = true;

    }
    else if (this.next == 5 && this.isDisabled5 == "false" && (!this.tutorTASE[0].url || !this.tutorTASE[0].name)) {
      this.bts = true;
      this.fl = true;


    }
    else if (this.next == 6 && !this.isDisabled55) {
      this.bts = true;

    }
    else if (this.next == 6 && this.isDisabled55 == "false" && (!this.tutorTOE[0].url)) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 6 && this.isDisabled55 == "false" && (!this.tutorTOE[0].name)) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 7 && !this.isDisabled51) {

      this.bts = true;

    }
    else if (this.next == 7 && this.isDisabled51 == "false" && (!this.tutorTE[0].name || !this.tutorTE[0].subjectTaught || !this.tutorTE[0].locationOfSchool || !this.tutorTE[0].gradeLvlOfStudent)) {
      this.bts = true;
      this.fl = true;


    }
    else if (this.next == 8 && (!this.tutor.highestDegree || !this.tutor.majorMinor)) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 8 && (!this.tutor.instname || !this.tutor.yearOfDegreeReceived)) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 8 && this.selectedValue.length == 0) {
      this.bts = true;


    }
    else if (this.next == 9 && !this.tutor.Verbal && !this.tutor.Speaking && !this.tutor.Reading1 && !this.tutor.Writing1) {
      this.bts = true;
      this.getCountry();
    }
    else if (this.next == 9 && !this.tutor.Reading1 && !this.tutor.Writing1) {
      this.bts = true;

    }
    else if (this.next == 9 && (!this.tutor.Writing1 || !this.tutor.Verbal || !this.tutor.Speaking)) {
      this.bts = true;

    }
    else if (this.next == 9 && !this.tutor.loc) {
      this.bts = true;

    }
    else {

      this.bts = false;
      this.fl = false;
      this.chk();
    }
  }

  backt() {
    if (this.next > 1)
      this.next = this.next - 1;
    this.chk();
  }

  goto(i) {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    if (!this.tutor.Firstname || !this.tutor.Lastname || !this.tutor.Residing) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (!this.tutor.dob) {
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 3 && !this.isDisabled) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 3 && this.isDisabled == "false" && (!this.invoiceForm.value.tutorTPAS[0].name || !this.invoiceForm.value.tutorTPAS[0].date || !this.invoiceForm.value.tutorTPAS[0].duration)) {
      this.bts = true;
      this.fl = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  name ,duration and date is required !!!`,
        timeout: 6000
      });


    }
    else if (i == 4 && !this.isDisabled3) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 4 && this.isDisabled3 == "false" && (!this.invoiceForm.value.tutorTTA[0].location || !this.invoiceForm.value.tutorTTA[0].date)) {
      this.bts = true;
      this.fl = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  location and date is required !!!`,
        timeout: 6000
      });

    }
    else if (i == 5 && this.isDisabled4 == "false" && (!this.invoiceForm.value.tutorTAE[0].location || !this.invoiceForm.value.tutorTAE[0].date)) {

      this.bts = true;
      this.fl = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill date and location also !!!`,
        timeout: 6000
      });

    }
    else if (i == 5 && !this.isDisabled4) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 6 && !this.isDisabled5) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 6 && this.isDisabled5 == "false" && (!this.invoiceForm.value.tutorTASE[0].url || !this.invoiceForm.value.tutorTASE[0].name)) {

      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill name and url also !!!`,
        timeout: 6000
      });
    }
    else if (i == 7 && !this.isDisabled55) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 7 && this.isDisabled55 == "false" && (!this.invoiceForm.value.tutorTOE[0].url || !this.invoiceForm.value.tutorTOE[0].name)) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill name and url also !!!`,
        timeout: 6000
      });
    }
    else if (i == 8 && !this.isDisabled51) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill  * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 8 && this.isDisabled51 == "false" && (!this.invoiceForm.value.tutorTE[0].subjectTaught || !this.invoiceForm.value.tutorTE[0].name || !this.invoiceForm.value.tutorTE[0].locationOfSchool || !this.invoiceForm.value.tutorTE[0].gradeLvlOfStudent)) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill name, location, greade level, Subject(s) Taught  !!!`,
        timeout: 6000
      });
    }
    else if (i == 9 && !this.tutor.highestDegree && !this.tutor.majorMinor && !this.tutor.instname && !this.tutor.yearOfDegreeReceived) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 9 && this.selectedValue.length == 0) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else if (i == 10 && !this.tutor.Verbal && !this.tutor.Speaking && !this.tutor.Reading1 && !this.tutor.Writing1) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill Verbal, Speaking, Reading, Writing field !!!`,
        timeout: 6000
      });
    }
    else if (i == 10 && !this.tutor.loc) {
      this.bts = true;
    }
    else if (i == 9 && !this.tutor.highestDegree && !this.tutor.majorMinor && !this.tutor.instname && !this.tutor.yearOfDegreeReceived) {
      this.bts = true;
      this.alerts.push({
        type: 'danger',
        msg: `Please fill all * required field !!!`,
        timeout: 6000
      });
    }
    else {
      this.next = i;
      this.bts = false;
      this.chk();

    }
  }

  chk() {
    if (!this.tutor.Firstname || !this.tutor.Lastname || !this.tutor.Residing || !this.tutor.dob) {
      this.bts = true;
    }
    else if (this.next == 2 && !this.isDisabled) {

      this.bts = true;

    }
    else if (this.next == 2 && this.isDisabled == "true") {

      this.bts = false;


      this.invoiceForm.patchValue({
        tutorTPAS: [{ name: [' '], date: '', duration: [' '] }]
      });


    }

    else if (this.next == 2 && this.isDisabled == "false" && (!this.invoiceForm.value.tutorTPAS[0].name || !this.invoiceForm.value.tutorTPAS[0].date || !this.invoiceForm.value.tutorTPAS[0].duration)) {
      this.bts = true;
    }
    else if (this.next == 3 && !this.isDisabled3) {
      this.bts = true;

    }
    else if (this.next == 3 && this.isDisabled3 == "true") {
      this.bts = false;

      this.invoiceForm.patchValue({
        tutorTTA: [{ date: '', location: [' '], accountNumber: [''] }]
      });
    }
    else if (this.next == 3 && this.isDisabled3 == "false" && (!this.invoiceForm.value.tutorTTA[0].location || !this.invoiceForm.value.tutorTTA[0].date)) {

      this.bts = true;



    }
    else if (this.next == 4 && this.isDisabled4 == "true") {
      this.bts = false;
      this.invoiceForm.patchValue({
        tutorTAE: [{ location: [' '], date: '' }]
      });
    }
    else if (this.next == 4 && this.isDisabled4 == "false" && (!this.invoiceForm.value.tutorTAE[0].location || !this.invoiceForm.value.tutorTAE[0].date)) {

      this.bts = true;
      this.fl = true;


    }

    else if (this.next == 4 && !this.isDisabled4) {
      this.bts = true;

    }
    else if (this.next == 5 && !this.isDisabled5) {
      this.bts = true;

    }
    else if (this.next == 5 && this.isDisabled5 == "true") {
      this.bts = false;
      this.invoiceForm.patchValue({
        tutorTASE: [{ url: [' '], name: [' '] }]
      });
    }
    else if (this.next == 5 && this.isDisabled5 == "false" && (!this.invoiceForm.value.tutorTASE[0].url || !this.invoiceForm.value.tutorTASE[0].name)) {
      this.bts = true;
      this.fl = true;
    }
    else if (this.next == 6 && !this.isDisabled55) {
      this.bts = true;

    }
    else if (this.next == 6 && this.isDisabled55 == "true") {
      this.bts = false;
      this.invoiceForm.patchValue({
        tutorTOE: [{ url: [' '], name: [' '] }]
      });
      this.fl = false;
    }
    else if (this.next == 6 && this.isDisabled55 == "false" && (!this.invoiceForm.value.tutorTOE[0].url || !this.invoiceForm.value.tutorTOE[0].name)) {
      this.bts = true;
    }
    else if (this.next == 7 && !this.isDisabled51) {
      this.bts = true;

    }
    else if (this.next == 7 && this.isDisabled51 == "true") {
      this.bts = false;
      this.fl = false;
      this.invoiceForm.patchValue({
        tutorTE: [{ subjectTaught: [' '], name: [' '], locationOfSchool: [' '], gradeLvlOfStudent: [' '] }]
      });
    }
    else if (this.next == 7 && this.isDisabled51 == "false" && (!this.invoiceForm.value.tutorTE[0].subjectTaught || !this.invoiceForm.value.tutorTE[0].name || !this.invoiceForm.value.tutorTE[0].locationOfSchool || !this.invoiceForm.value.tutorTE[0].gradeLvlOfStudent)) {
      this.bts = true;
      this.fl = true;

    }
    else if (this.next == 8 && !this.tutor.highestDegree && !this.tutor.majorMinor && !this.tutor.instname && !this.tutor.yearOfDegreeReceived) {
      this.bts = true;
    }
    else if (this.next == 8 && this.selectedValue.length == 0) {
      this.bts = true;
    }
    else if (this.next == 9 && (!this.tutor.Verbal || !this.tutor.Speaking) && (!this.tutor.Reading1 || !this.tutor.Writing1)) {
      this.bts = true;
    }
    else if (this.next == 9 && (!this.tutor.loc || this.tutor.loc == undefined)) {
      this.bts = true;
    }
    else if (this.next == 8 && !this.tutor.highestDegree && !this.tutor.majorMinor && !this.tutor.instname && !this.tutor.yearOfDegreeReceived) {
      this.bts = true;
    }
    else {

      this.bts = false;
    }

  }

  reset() {
    window.location.reload();
  }
  /************************************************************* */

  addNewTutor(s) {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];


    if (!s) {

      this.goto(1);
      this.alerts.push({
        type: 'danger',
        msg: `Please enter your Fullname !!!`,
        timeout: 6000
      });
    }


    else if (!s.location1) {
      s.location1 = " ";
    }

    else {
      s.dob = this.datePipe.transform(s.dob, 'yyyy-MM-dd');

      if (this.selectedValue) {
        for (let selecteds in this.selectedValue) {
          this.subject1.push({
            "section": this.selectedValue[selecteds]

          });
        }
      }
      if (this.selectedValue1) {
        for (let selecteds in this.selectedValue1) {
          this.aceept.push({
            "declareType": this.selectedValue1[selecteds]

          });
        }
      }

      let tutor1 = {
        "fullName": s.Firstname + ' ' + s.Lastname,
        "residingAddress": s.Residing,
        "dob": s.dob,
        "highestDegree": s.highestDegree,
        "majorMinor": s.majorMinor,
        "institutionOfDegree": s.instname,
        "yearOfDegreeReceived": s.yearOfDegreeReceived,
        "registeringLocation": s.loc,
        "status": "INACTIVE",
        "tutorTPA": this.invoiceForm.value.tutorTPA,
        "tutorTEA": this.invoiceForm.value.tutorTEA,
        "tutorTPN": this.invoiceForm.value.tutorTPN,
        "tutorTPAS": this.invoiceForm.value.tutorTPAS,
        "tutorTTA": this.invoiceForm.value.tutorTTA,
        "tutorTAE": this.invoiceForm.value.tutorTAE,
        "tutorTASE": this.invoiceForm.value.tutorTASE,
        "tutorTOE": this.invoiceForm.value.tutorTOE,
        "tutorTE": this.invoiceForm.value.tutorTE,
        "tutorTCS": this.subject1,
        "tutorTKL": [
          {
            "language": "English",
            "abilityType": "Speaking",
            "proficiency": s.Speaking
          },
          {
            "language": "English",
            "abilityType": "Writing",
            "proficiency": s.Writing1
          },
          {
            "language": "English",
            "abilityType": "reading",
            "proficiency": s.Reading1
          },
          {
            "language": "English",
            "abilityType": "Verbal Comprehension",
            "proficiency": s.Verbal
          }
        ],
        "plmUser": {
          "id": sessionStorage.uid
        },
        "country": {
          "id": this.counrty
        },
        "futureRefLocation": s.location1,
        "futureRefStartDate": s.Start,
        "futureRefEndDate": s.End,
        "tutorTA": this.aceept
      };

      this.alerts.push({
        type: 'success',
        msg: `Loading Please wait ...`,
        timeout: 10000
      });


    }

  }

  addn2(s) {

    this.modalRef.hide()
    if (!s) {

      this.goto(1);
      this.alerts.push({
        type: 'danger',
        msg: `Please enter your Fullname !!!`,
        timeout: 6000
      });
    }


    else if (!s.location1) {
      s.location1 = " ";
    }

    else {
      s.dob = this.datePipe.transform(s.dob, 'yyyy-MM-dd');

      if (this.tutor.fcounrty == "sel") {
        this.tutor.fcounrty = 0;
      }
      if (this.tutor.cityidss == "sel") {
        this.tutor.cityidss = 0;

      }
      if (this.selectedValue) {
        for (let selecteds in this.selectedValue) {
          this.subject1.push({
            "section": this.selectedValue[selecteds]

          });
        }
      }
      if (this.selectedValue1) {
        for (let selecteds in this.selectedValue1) {
          this.aceept.push({
            "declareType": this.selectedValue1[selecteds]

          });
        }
      }

      let tutor1 = {
        "fullName": s.Firstname + ' ' + s.Lastname,
        "firstName": s.Firstname,
        "lastName": s.Lastname,
        "residingAddress": s.Residing,
        "dob": s.dob,
        "highestDegree": s.highestDegree,
        "majorMinor": s.majorMinor,
        "institutionOfDegree": s.instname,
        "yearOfDegreeReceived": s.yearOfDegreeReceived,
        "registeringLocation": s.loc,
        "status": "INACTIVE",
        "tutorTPA": this.invoiceForm.value.tutorTPA,
        "tutorTEA": this.invoiceForm.value.tutorTEA,
        "tutorTPN": this.invoiceForm.value.tutorTPN,
        "tutorTPAS": this.invoiceForm.value.tutorTPAS,
        "tutorTTA": this.invoiceForm.value.tutorTTA,
        "tutorTAE": this.invoiceForm.value.tutorTAE,
        "tutorTASE": this.invoiceForm.value.tutorTASE,
        "tutorTOE": this.invoiceForm.value.tutorTOE,
        "tutorTE": this.invoiceForm.value.tutorTE,
        "tutorTCS": this.subject1,
        "tutorTKL": [
          {
            "language": "English",
            "abilityType": "Speaking",
            "proficiency": s.Speaking
          },
          {
            "language": "English",
            "abilityType": "Writing",
            "proficiency": s.Writing1
          },
          {
            "language": "English",
            "abilityType": "reading",
            "proficiency": s.Reading1
          },
          {
            "language": "English",
            "abilityType": "Verbal Comprehension",
            "proficiency": s.Verbal
          }
        ],
        "plmUser": {
          "id": sessionStorage.uid
        },
        "country": {
          "id": this.counrty
        },
        "city": {
          "id": this.cityids
        },
        "refCountry": this.tutor.fcounrty,
        "refCity": this.tutor.cityidss,
        "tutorTrainers": [{ "trainerId": this.Trainerss }],
        "futureRefLocation": s.location1,
        "futureRefStartDate": s.Start,
        "futureRefEndDate": s.End,
        "tutorTA": this.aceept
      };

      this.alerts.push({
        type: 'success',
        msg: `Submitting the form.....`,
        timeout: 10000
      });
      this._demoService.createTutordata(tutor1).subscribe(
        data => {

          this.tutor1 = data;

          sessionStorage.newtutor = JSON.stringify(this.tutor1);

          this.selectedValue = [];
          this.subject1 = [];
          this.selectedValue1 = [];
          this.aceept = [];


          this.router.navigate(['/tutorwel']);

        },
        error => {

          this.selectedValue = [];
          this.selectedValue1 = [];



        }


      )

    }

  }

  getCountry() {
    this.fl1 = true;
    let url = "/plm/countriesBytrainers?filter=%7B%7D";
    this._demoService.getfcountry(url).subscribe(
      data => {
        this._countrys = data;
      },
      error => {
        alert("Server Error!!!");

        this.f1 = false;
      },

    )
  }

  getcitybycountry(cid) {

    this._demoService.getfcitybycid(cid).subscribe(
      data => {
        this.city1 = data;

        this.fl1 = false;


      },
      error => {
        alert("Server Error!!!");

        this.f1 = false;
      },

    )
  }

  getFutureCountry() {
    this.fl1 = true;

    this._demoService.gettutorcountry().subscribe(
      data => {
        this._countrys1 = data;


      },
      error => {

        alert("Server Error!!!");
        this.f1 = false;
      },

    )

  }

  getcitybyFuturecountry(cid) {

    this._demoService.getcitybycid(cid).subscribe(
      data => {
        this.cityf1 = data;

        this.fl1 = false;

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));
        this.f1 = false;
      },

    )
  }


  gettrinerbyCity(couid, cityid) {

    this._demoService.gettrinerbyCity(couid, cityid).subscribe(
      data => {
        this.trainer = data;

        this.fl1 = false;

      },
      error => {


        alert(JSON.stringify(error.error.meta.error_message));
        this.f1 = false;
      },

    )
  }

  chkdate(){
    
    if(this.tutor.End<this.tutor.Start){
      this.alerts.push({
        type: 'danger',
        msg: 'TraningEndDate Should be greater than TraningStartDate !',
        timeout: 5000
      });
    }

  }

 

  

  
}
