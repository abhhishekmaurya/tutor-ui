import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeadervComponent } from '../headerv/headerv.component';
import { NewTutorComponent } from './new-tutor.component';
import { AlertModule, ModalModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap';

describe('NewTutorComponent', () => {
  let component: NewTutorComponent;
  let fixture: ComponentFixture<NewTutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientModule,AlertModule.forRoot(),FormsModule,ModalModule.forRoot(),ReactiveFormsModule,BsDatepickerModule.forRoot()], 
      declarations: [ NewTutorComponent,HeadervComponent ],
      providers : [MyserviceService,AuthGuard]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
