
import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// Import RxJs required methods
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-tutor',
  templateUrl: './tutor.component.html',
  styleUrls: ['./tutor.component.css']
})

export class TutorComponent implements OnInit {
  counrty: any;
  percentDone1: any;
  @ViewChild('fileImportInput') fileImportInput: ElementRef;
  msg: string;
  percentage: number;
  public next = 1;
  errm: any;
  fildata: any;
  fileToUpload: File = null;
  dtOptions: any = {};
  public temp_var: Object = false;
  public tutor: any = {

  };
  datePipe = new DatePipe("en-US");
  // step=2;
  selectedFiles: FileList;
  modalRef: any;
  alerts1: { type: string; msg: string; timeout: number; }[];
  food1: any;
  bsModalRef: BsModalRef;
  user: string[];
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  dismissible = true;
  ageants1: any;
  public step = 1;

  constructor(private _fb: FormBuilder, private _demoService: MyserviceService, private modalService: BsModalService, private http: HttpClient) {



  }

  ngOnInit() {
    this.getTutordata();
    this.dtOptions = {
      paging: true,
      pageLength: 10,
      processing: true,
      columnDefs: [ {

        targets: [0,4,8], // column or columns numbers
        
        orderable: false,  // set orderable for selected columns
        
        }]
    };
  }



  username = this._demoService.username;

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  openModal(template: TemplateRef<any>, f) {
    sessionStorage.models = 'true';
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.food1 = f;
    sessionStorage.tid = f.id;
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'tutor_data_model' }));

  }

  openModal1(template: TemplateRef<any>) {

    sessionStorage.models = 'true';
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.modalRef = this.modalService.show(template);

  }


  openModalWithComponent(template: TemplateRef<any>, f) {
    sessionStorage.models = 'true';
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.food1 = f;
    this.food1.firstName = f.firstName;
    this.modalRef = this.modalService.show(template);
  }


  add(): void {

    this.alerts.push({
      type: 'info',
      msg: `This alert will be closed in 5 seconds (added: ${new Date().toLocaleTimeString()})`,
      timeout: 5000
    });
  }

  public changeListener(files: FileList) {
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.percentDone1 = 0;

    if (!this.isCSVFile(files[0])) {
      this.alerts1.push({
        type: 'danger',
        msg: 'Please import valid .csv file..',
        timeout: 2000
      });
      this.fileReset(files[0]);
    }
    else {

      this.fileToUpload = files.item(0);
      if (files && files.length > 0) {

        this.selectedFiles = files;
        let file: File = files.item(0);


        let reader: FileReader = new FileReader();
        reader.readAsText(file);
        reader.onload = (e) => {

          let csv: string = reader.result;


        }

      }
    }
  }

  ngOnDestroy() {
    if (sessionStorage.models == 'true') {

      this.modalRef.hide();
      sessionStorage.models = false;
    }
  }

  upload() {
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.alerts1.push({
      type: 'success',
      msg: 'Uploading..',
      timeout: 2000
    });

    this._demoService.postFile(this.fileToUpload).subscribe(

      data => {
        // do something, if upload success
        this.alerts1 = [{
          type: '',
          msg: ``,
          timeout: 5000
        }];
        this.fildata = data;
        this.modalRef.hide();
        alert(this.fildata.data);

        setTimeout(() => {
          window.location.reload();
        }, 800);
        this.getTutordata();

        this.alerts1.push({
          type: 'success',
          msg: this.fildata.data,
          timeout: 5000
        });

      }
      , error => {
        this.alerts1 = [{
          type: '',
          msg: ``,
          timeout: 5000
        }];
        this.alerts1.push({
          type: 'danger',
          msg: `Error In File Upload!!!`,
          timeout: 5000
        });

      });
  }
  
  fileReset(files) {

  }
  
  isCSVFile(file) {
    return file.name.endsWith(".csv");
  }

  getTutordata() {
    let url = "/plm/tutors?filter=%7B%7D";
    this._demoService.getfcountry(url).subscribe(
      data => {
        this.ageants1 = data;
        this.ageants1 = this.ageants1.data;
        var name, name1;
        this.temp_var = true;
        for (let t in this.ageants1.data) {
          name = this.ageants1.data[t].fullName.split(" ");

        }

      },
      error => {
        alert(JSON.stringify(error.error.meta.error_message));
        return Observable.throw(error);
      },

    )

  }

  importTutordata(template: TemplateRef<any>) {
    sessionStorage.models = 'true';
    this.selectedFiles = undefined;
    this.percentDone1 = 0;
    this.msg = "";
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.modalRef = this.modalService.show(template);

  }

  exportTutordata() {
    var tutord;
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true
    };
    let url = "/plm/tutors?filter=%7B%7D";
    this._demoService.getfcountry(url).subscribe(

      data => {
        this.ageants1 = data;
        tutord = JSON.stringify(this.ageants1.data);
        new Angular5Csv(tutord, 'Mytutorreport', options);
      },
      error => {
        alert(JSON.stringify(error.error.meta.error_message));

      },


    )

  }

  changesatus(s, st) {
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this._demoService.changestatus(s, st).subscribe(
      data => {
        this.ageants1 = data;

        this.getTutordata();
        this.alerts.push({
          type: 'success',
          msg: this.ageants1.data[0],
          timeout: 5000
        });
        this.modalRef.hide();

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));
        return Observable.throw(error);
      },

    )
  }



  /****************************************chkbox check and uncheck*****************************************************/
  checkAll(ev) {

    this.ageants1.forEach(x => x.state = ev.target.checked);

  }

  isAllChecked() {

    if (this.ageants1) {
      return this.ageants1.every(_ => _.state);
    }
  }



}

