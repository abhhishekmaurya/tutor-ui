import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TutorComponent } from './tutor.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule, ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { HeadervComponent } from '../headerv/headerv.component';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';
import { ViewtutorComponent } from '../viewtutor/viewtutor.component';


describe('TutorComponent', () => {
  let component: TutorComponent;
  let fixture: ComponentFixture<TutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientModule,AlertModule.forRoot(),FormsModule,ModalModule.forRoot(),BsDatepickerModule.forRoot(),ReactiveFormsModule,DataTablesModule],
      declarations: [ TutorComponent,HeadervComponent,ViewtutorComponent],
      providers : [MyserviceService,AuthGuard]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
