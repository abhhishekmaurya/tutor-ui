import { TestBed, inject } from '@angular/core/testing';
import { MyserviceService } from './myservice.service';
import {HttpClientModule} from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterModule } from '@angular/router';

describe('MyserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports : [HttpClientModule, RouterModule, RouterTestingModule],
      providers: [MyserviceService]
    });
  });

  it('should be created', inject([MyserviceService], (service: MyserviceService) => {
    expect(service).toBeTruthy();
  }));
});
