import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Appconfig } from '../appconfig';
import { DOCUMENT } from '@angular/platform-browser';

const httpOptions = {
  
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class MyserviceService {
  token1: string;

  host11: any;
  endpoint: any;
  private config: Appconfig
  constructor(@Inject(DOCUMENT) private document, private http: HttpClient) {
    if (!this.token) {
      this.token = "d4etmfBQ-EsarzuoGSdCVm981IVn4ZS8ZPfYfmJNuRcIDLrKdct_U_NAxqSnQMh0MSh0OZcJ4hxbKiy89_KV0isKLQsH0BbP32AWd5gAB7k";
    }


    let url = document.location.protocol + '//' + document.location.hostname + ':9000';

    this.host11 = url;
    this.endpoint = this.host11;
    // this.endpoint=Globalsetting.BASE_URL;
    this.endpoint = "http://34.194.80.148:9000";
    // this.endpoint="http://18.204.69.116:9000";

    sessionStorage.ep = this.endpoint;


  }


  username;
  // endpoint;




  token = sessionStorage.data;

  signout() {

    this.token = "d4etmfBQ-EsarzuoGSdCVm981IVn4ZS8ZPfYfmJNuRcIDLrKdct_U_NAxqSnQMh0MSh0OZcJ4hxbKiy89_KV0isKLQsH0BbP32AWd5gAB7k";
    sessionStorage.username = "";
    sessionStorage.usertype = 'USER';
    sessionStorage.data = "d4etmfBQ-EsarzuoGSdCVm981IVn4ZS8ZPfYfmJNuRcIDLrKdct_U_NAxqSnQMh0MSh0OZcJ4hxbKiy89_KV0isKLQsH0BbP32AWd5gAB7k";

  }

  createTutordata(Agents) {

    let body = JSON.stringify(Agents);
    return this.http.post(this.endpoint + '/plm/tutor?userId=' + this.token, body, httpOptions);
  }

  updateTutordata(Agents) {

    let body = JSON.stringify(Agents);
    return this.http.put(this.endpoint + '/plm/tutor?userId=' + this.token, body, httpOptions);
  }

  deleteTutordata(Agents) {

    return this.http.delete(this.endpoint + '/plm/tutor?ids=' + Agents.id + '&userId=' + this.token);
  }

  gettutorcountry() {

    let filterc = { "order": { "countryName": "ASC" } };
    let filterc1 = JSON.stringify(filterc);

    return this.http.get(this.endpoint + '/plm/countries?filter=' + filterc1 + '&userId=' + this.token);
  }

  gettrinerbyCity(countid, cityid) {
    return this.http.get(this.endpoint + '/plm/trainerByCityNCountry?filter=' + '%7B%22order%22%3A%7B%22firstName%22%3A%22ASC%22%7D%7D' + '&countryId=' + countid + '&cityId=' + cityid + '&userId=' + this.token);
  }

  getcitybycid(id) {
    let filterc = { "order": { "cityName": "ASC" } };
    let filterc1 = JSON.stringify(filterc);
    return this.http.get(this.endpoint + '/plm/citiesByCountry?filter=' + filterc1 + '&countryId=' + id + '&userId=' + this.token);
  }
  /************************************user************************ */
  createuser(s) {
    this.token1 = "d4etmfBQ-EsarzuoGSdCVm981IVn4ZS8ZPfYfmJNuRcIDLrKdct_U_NAxqSnQMh0MSh0OZcJ4hxbKiy89_KV0isKLQsH0BbP32AWd5gAB7k";
    let body = JSON.stringify(s);
    return this.http.post(this.endpoint + '/plm/plmUser?userId=' + this.token1, body, httpOptions);
  }

  loginuser(Agents) {
    this.token1 = "d4etmfBQ-EsarzuoGSdCVm981IVn4ZS8ZPfYfmJNuRcIDLrKdct_U_NAxqSnQMh0MSh0OZcJ4hxbKiy89_KV0isKLQsH0BbP32AWd5gAB7k";
    let body = JSON.stringify(Agents);
    return this.http.post(this.endpoint + '/plm/loginUser?userId=' + this.token1, body, httpOptions);
  }

  loginuserrole(Agents) {


    return this.http.get(this.endpoint + '/plm/tutorByUser?plmUserId=' + sessionStorage.uid + '&userId=' + this.token);
  }

  changestatus(Agents, st) {

    let body = JSON.stringify(Agents);
    return this.http.put(this.endpoint + '/plm/changeTutorStatus?id=' + Agents.id + '&status=' + st + '&userId=' + this.token, body, httpOptions);
  }

  resetpass(Agents) {


    let body = JSON.stringify(Agents);
    return this.http.put(this.endpoint + '/plm/resetPassword?userId=' + this.token, body, httpOptions);
  }

  forgetpass(Agents) {
    let body = JSON.stringify(Agents);
    return this.http.put(this.endpoint + '/plm/forgotPassword?userId=' + this.token, body, httpOptions);

  }

  gettutorbyid(id) {
    return this.http.get(this.endpoint + '/plm/tutor?tutorId=' + id + '&userId=' + this.token);
  }
  /***********************************************************************************************/
  getfcountry(url) {
    return this.http.get(this.endpoint + url + '&userId=' + this.token);
  }

  getfcitybycid(id) {
    let filterc = { "order": { "cityName": "ASC" } };
    let filterc1 = JSON.stringify(filterc);
    return this.http.get(this.endpoint + '/plm/citiesBytrainers?filter=' + filterc1 + '&countryId=' + id + '&userId=' + this.token);
  }


  postdata(url, body) {
    let body1 = JSON.stringify(body);
    return this.http.post(this.endpoint + url + this.token, body1, httpOptions);
  }

  delete(url) {

    return this.http.delete(this.endpoint + url + '&userId=' + this.token);
  }

  edits(url, Agents) {
    let body = JSON.stringify(Agents);
    return this.http.put(this.endpoint + url + '?userId=' + this.token, body, httpOptions);
  }

  postFile(fileToUpload: File) {

    const formData: FormData = new FormData();
    formData.append('csvFile', fileToUpload, fileToUpload.name);
    return this.http.post(this.endpoint + '/plm/upload?userId=' + this.token, formData);

  }

}
