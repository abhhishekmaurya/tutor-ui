import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NewpassComponent } from './newpass.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule, ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';
describe('NewpassComponent', () => {
  let component: NewpassComponent;
  let fixture: ComponentFixture<NewpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, AlertModule.forRoot(), FormsModule, ModalModule.forRoot(), ReactiveFormsModule, BsDatepickerModule.forRoot()],
      declarations: [NewpassComponent],
      providers: [MyserviceService, AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
