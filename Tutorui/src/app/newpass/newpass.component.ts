import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { Router } from '@angular/router';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
  // headers: new HttpHeaders({ 'Content-Type': '*/*' })
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-newpass',
  templateUrl: './newpass.component.html',
  styleUrls: ['./newpass.component.css']
})

export class NewpassComponent implements OnInit {

  pass21: string;
  pass11: string;
  tutor1: Object;
  data1: any;
  token: string | number;
  userId: string | number;
  host: any;
  userid: any;
  dismissible = true;
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  constructor(private http: HttpClient, private router: Router, private _demoService: MyserviceService) {

  }

  ngOnInit() {
    this.host = window.location.href;

    this.userId = this.GetParam('userId');
    this.token = this.GetParam('token');


  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  GetParam(name) {
    const results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (!results) {
      return 0;
    }
    return results[1] || 0;
  }

  Changepass(p1, p2) {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    if (!p1) {

      this.alerts.push({
        type: 'danger',
        msg: 'Please Enter New Password.',
        timeout: 5000
      });
    }
    else if (!p2) {

      this.alerts.push({
        type: 'danger',
        msg: 'Please Enter Confirm Password.',
        timeout: 5000
      });
    }
    else if (p1 != p2) {
      this.alerts.push({
        type: 'danger',
        msg: `Password and Confirm Password should be same !!!`,
        timeout: 5000
      });
    } else {
      let Agents = {
        "userPassword": p1,
        "email": ""
      }

      this.resetpass(Agents).subscribe(
        data => {
          this.tutor1 = data;
          this.alerts.push({
            type: 'success',
            msg: 'New Password updated successfully.',
            timeout: 5000
          });
          alert("New Password updated successfully.");
          this.router.navigate(['/login']);
          this.pass11 = "";
          this.pass21 = "";
        },

        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.error.meta.error_message,
            timeout: 5000
          });
        }
      )

    }

  }

  resetpass(Agents) {
    let body = JSON.stringify(Agents);
    return this.http.put(this._demoService.endpoint + '/plm/resetPassword?userId=' + this.userId + '&token=' + this.token, body, httpOptions);
  }
}
