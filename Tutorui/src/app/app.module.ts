import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { MyserviceService } from '../app/services/myservice.service';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { ViewagentComponent } from './viewagent/viewagent.component';
import { HeadervComponent } from './headerv/headerv.component';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestdateComponent } from './testdate/testdate.component';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap';
import { TutorComponent } from './tutor/tutor.component';
import { NewTutorComponent } from './new-tutor/new-tutor.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { Appconfig } from './appconfig';
import { TutorlandComponent } from './tutorland/tutorland.component';
import { DataTablesModule } from 'angular-datatables';
import { NewpassComponent } from './newpass/newpass.component';
import { ViewtutorComponent } from './viewtutor/viewtutor.component';
import { AuthGuard } from './auth.guard';



@NgModule({
  declarations: [
    AppComponent,
    ViewagentComponent,
    HeadervComponent,
    TestdateComponent,
    TutorComponent,
    NewTutorComponent,
    LoginComponent,
    SignupComponent,
    TutorlandComponent,
    NewpassComponent,
    ViewtutorComponent
  ],
  imports: [
    BrowserModule, NgSelectModule,
    FormsModule, ReactiveFormsModule,
    DataTablesModule,
    HttpClientModule,
    AppRoutingModule,
    ModalModule.forRoot(), BsDatepickerModule.forRoot(), AlertModule.forRoot()
  ],
  providers: [MyserviceService, Appconfig, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
