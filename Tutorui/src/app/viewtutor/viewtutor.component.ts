import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MyserviceService } from '../services/myservice.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { TutorComponent } from '../tutor/tutor.component';
@Component({
  selector: 'app-viewtutor',
  templateUrl: './viewtutor.component.html',
  styleUrls: ['./viewtutor.component.css']
})
export class ViewtutorComponent implements OnInit {
  counrty: any;
  modalRef: BsModalRef;
  public invoiceForm: FormGroup;
  ageants1: any;
  public next = 1;
  // public tutor :any={

  // };
  public tutor: any = [];
  @ViewChild("component1") component1: TutorComponent;
  datePipe = new DatePipe("en-US");
  constructor(private _fb: FormBuilder, private _demoService: MyserviceService, private modalService: BsModalService) {

    this.getdata(sessionStorage.tid);
  }

  ngOnInit() {
    this.invoiceForm = this._fb.group({
      tutorTPA: this._fb.array([this.initItemRows()]),
      tutorTEA: this._fb.array([this.initItemRows1()]),
      tutorTPN: this._fb.array([this.initItemRows2()]),
      tutorTPAS: this._fb.array([this.initItemRows3()]),
      tutorTTA: this._fb.array([this.initItemRows4()]),
      tutorTAE: this._fb.array([this.initItemRows5()]),
      tutorTASE: this._fb.array([this.initItemRows6()]),
      tutorTOE: this._fb.array([this.initItemRows7()]),
      tutorTE: this._fb.array([this.initItemRows8()])
    });


  }



  initItemRows() {
    return this._fb.group({
      address: ['']
    });
  }
  initItemRows1() {
    return this._fb.group({
      "email": ['', Validators.pattern],
      "typeOfAddress": ['1']

    });
  }
  initItemRows2() {
    return this._fb.group({
      "phoneNumber": [''],
      "typeOfNumber": ['1']

    });
  }

  initItemRows3() {
    return this._fb.group({
      "name": [''],
      "date": [''],
      "duration": ['']


    });
  }
  
  initItemRows4() {
    return this._fb.group({
      "date": [''],
      "location": [''],
      "accountNumber": ['']
    });


  }
  initItemRows5() {
    return this._fb.group({
      "date": [''],
      "location": ['']
    });


  }
  initItemRows6() {
    return this._fb.group({
      "name": [''],
      "url": ['']
    });
  }

  initItemRows7() {
    return this._fb.group({
      "name": [''],
      "url": ['']
    });
  }

  initItemRows8() {
    return this._fb.group({
      "name": [''],
      "locationOfSchool": [''],
      "gradeLvlOfStudent": [''],
      "subjectTaught": ['']
    });


  }

  goto(i) {
    this.next = i;

  }

  nextt() {

    if (this.next < 12) {
      this.next = this.next + 1;
    }
  }

  backt() {
    if (this.next > 1)
      this.next = this.next - 1;
  }

  getdata(id) {
    this._demoService.gettutorbyid(id).subscribe(
      data => {
        this.ageants1 = data;
        this.tutor.Firstname = this.ageants1.data[0].fullName;
        this.tutor.remail = this.ageants1.data[0].plmUser.email;
        this.ageants1.data[0].dob = new Date(this.ageants1.data[0].dob);
        this.tutor.dob = this.datePipe.transform(this.ageants1.data[0].dob, 'MM/dd/yyyy');
        this.tutor.Residing = this.ageants1.data[0].residingAddress;
        this.tutor.tutorTPA1 = this.ageants1.data[0].tutorTPA;
        this.tutor.tutorTEA1 = this.ageants1.data[0].tutorTEA;
        this.tutor.tutorTPN1 = this.ageants1.data[0].tutorTPN;
        if (this.ageants1.data[0].tutorTPAS.length != 0) {

          this.tutor.tutorTPAS1 = this.ageants1.data[0].tutorTPAS;
          this.tutor.ff = "true";
        }
        else {

          this.tutor.ff = "false";
          this.tutor.tutorTPAS1 = this.ageants1.data[0].tutorTPAS;
        }
        if (this.ageants1.data[0].tutorTTA.length != 0) {
          this.tutor.tutorTTA = this.ageants1.data[0].tutorTTA;
          this.tutor.ff1 = "true";
        }
        else {
          this.tutor.ff1 = "false";
          this.tutor.tutorTTA = this.ageants1.data[0].tutorTTA;
        }

        if (this.ageants1.data[0].tutorTAE.length != 0) {
          this.tutor.tutorTAE = this.ageants1.data[0].tutorTAE;
          this.tutor.f11 = "true";
        }
        else {
          this.tutor.f11 = "false";
          this.tutor.tutorTAE = this.ageants1.data[0].tutorTAE;
        }
        if (this.ageants1.data[0].tutorTASE.length != 0) {
          this.tutor.tutorTASE = this.ageants1.data[0].tutorTASE;
          this.tutor.f12 = "true";

        }
        else {
          this.tutor.f12 = "false";
          this.tutor.tutorTASE = this.ageants1.data[0].tutorTASE;
        }

        if (this.ageants1.data[0].tutorTOE.length != 0) {
          this.tutor.tutorTOE = this.ageants1.data[0].tutorTOE;
          this.tutor.isDisabled55 = "true";
        }
        else {
          this.tutor.isDisabled55 = "false";
          this.tutor.tutorTOE = this.ageants1.data[0].tutorTOE;
        }
        if (this.ageants1.data[0].tutorTE.length != 0) {
          this.tutor.tutorTE = this.ageants1.data[0].tutorTE;
          this.tutor.isDisabled51 = "true";

        }
        else {
          this.tutor.isDisabled51 = "false";
          this.tutor.tutorTE = this.ageants1.data[0].tutorTE;

        }
        this.tutor.highestDegree = this.ageants1.data[0].highestDegree;
        this.tutor.majorMinor = this.ageants1.data[0].majorMinor;
        this.tutor.instname = this.ageants1.data[0].institutionOfDegree;
        this.tutor.yearOfDegreeReceived = this.ageants1.data[0].yearOfDegreeReceived;
        if (this.ageants1.data[0].tutorTCS) {

          for (let selecteds in this.ageants1.data[0].tutorTCS) {

            if (this.ageants1.data[0].tutorTCS[selecteds].section == "Basics") {
              this.tutor.Basics = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Writing") {
              this.tutor.Writing = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Reading") {
              this.tutor.Reading = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "English") {
              this.tutor.english = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Math") {
              this.tutor.Math = this.ageants1.data[0].tutorTCS[selecteds].section;

            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Science") {

              this.tutor.Science = this.ageants1.data[0].tutorTCS[selecteds].section;
            }

          }
          if ((this.tutor.Basics && this.tutor.Writing) && (this.tutor.Reading && this.tutor.english) && (this.tutor.Math && this.tutor.Science)) {
            this.tutor.Allsectionofact = "Allsectionofact";
          }
        }

        for (let selct in this.ageants1.data[0].tutorTKL) {

          if (this.ageants1.data[0].tutorTKL[selct].abilityType == "Speaking") {
            this.tutor.Speaking = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
          else if (this.ageants1.data[0].tutorTKL[selct].abilityType == "Writing") {
            this.tutor.Writing1 = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
          else if (this.ageants1.data[0].tutorTKL[selct].abilityType == "reading") {
            this.tutor.Reading1 = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
          else if (this.ageants1.data[0].tutorTKL[selct].abilityType == "Verbal Comprehension") {
            this.tutor.Verbal = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
        }

        this.tutor.loc = this.ageants1.data[0].registeringLocation;
        this.tutor.fcountry = this.ageants1.data[0].refCountryName;
        this.tutor.fcity = this.ageants1.data[0].refCityName;
        this.tutor.location1 = this.ageants1.data[0].futureRefLocation;
        this.counrty = this.ageants1.data[0].country.countryName;
        this.tutor.city = this.ageants1.data[0].registeringLocation.split(" ")[0];
        if (this.ageants1.data[0].tutorTrainers.length != 0) {
          this.tutor.Trainers = this.ageants1.data[0].tutorTrainers[0].trainerName;

          this.tutor.Address = this.ageants1.data[0].tutorTrainers[0].trainerAddress;
        }
        this.tutor.tutorapp1 = this.ageants1.data[0].tutorTA[0].declareType;
        this.tutor.tutorapp2 = this.ageants1.data[0].tutorTA[1].declareType;
        this.tutor.tutorapp3 = this.ageants1.data[0].tutorTA[2].declareType;
        this.tutor.tutorapp4 = this.ageants1.data[0].tutorTA[3].declareType;
        this.tutor.tutorapp5 = this.ageants1.data[0].tutorTA[4].declareType;
        this.tutor.tutorapp6 = this.ageants1.data[0].tutorTA[5].declareType;
        this.tutor.tutorapp7 = this.ageants1.data[0].tutorTA[6].declareType;
        this.tutor.tutorapp8 = this.ageants1.data[0].tutorTA[7].declareType;
        this.tutor.tutorapp9 = this.ageants1.data[0].tutorTA[8].declareType;
        this.tutor.tutorapp10 = this.ageants1.data[0].tutorTA[9].declareType;
        this.tutor.tutorapp11 = this.ageants1.data[0].tutorTA[10].declareType;
        this.tutor.tutorapp12 = this.ageants1.data[0].tutorTA[11].declareType;

        this.tutor.Start = this.datePipe.transform(this.ageants1.data[0].futureRefStartDate, 'MM-DD-YYYY');
        this.tutor.End = this.datePipe.transform(this.ageants1.data[0].futureRefEndDate, 'MM-DD-YYYY');

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));

      },

    )

  }



}


