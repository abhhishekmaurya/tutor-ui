import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ViewagentComponent } from './viewagent/viewagent.component';
import { TestdateComponent } from './testdate/testdate.component';
import { TutorComponent } from './tutor/tutor.component';
import { NewTutorComponent } from './new-tutor/new-tutor.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { TutorlandComponent } from './tutorland/tutorland.component';
import { NewpassComponent } from './newpass/newpass.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', component: ViewagentComponent, canActivate: [AuthGuard ]},
  {path:'viewallTestdate',component:TestdateComponent,canActivate: [AuthGuard ]},
  {path:'allTutor',component:TutorComponent,canActivate: [AuthGuard ]},
  {path:'Newtutor',component:NewTutorComponent,canActivate: [AuthGuard ]},
  {path:'newUser',component:SignupComponent},
  {path:'login',component:LoginComponent},
  {path:'tutorwel',component:TutorlandComponent,canActivate: [AuthGuard ]},
  {path:'newpass',component:NewpassComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes,{ useHash: true})
  ],
  exports: [ RouterModule ],
  declarations: []
})

export class AppRoutingModule { 

  
}
