import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeadervComponent } from './headerv.component';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';
import { HttpClientModule } from '@angular/common/http';
describe('HeadervComponent', () => {
  let component: HeadervComponent;
  let fixture: ComponentFixture<HeadervComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [HeadervComponent],
      providers: [MyserviceService, AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadervComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
