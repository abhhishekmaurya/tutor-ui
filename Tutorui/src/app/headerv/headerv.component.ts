import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';

@Component({
  selector: 'app-headerv',
  templateUrl: './headerv.component.html',
  styleUrls: ['./headerv.component.css']
})
export class HeadervComponent implements OnInit {
  usertype: any;
  public username: any;
  constructor(private _demoService: MyserviceService) { }

  ngOnInit() {
    this.username = sessionStorage.username;
    this.usertype = sessionStorage.usertype;
    //  alert( this.usertype);
  }
  signout() {
    // alert("SDf");
    this._demoService.signout();
  }
}
