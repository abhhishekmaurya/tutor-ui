import { Component } from '@angular/core';
import { MyserviceService } from '../app/services/myservice.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'app';
  foods = [
    {
      "id": 1,
      "firstName": "Mithun",
      "lastName": "Mondal",
      "address": "String",
      "email": "String",
      "country": "String",
      "company": "String",
      "status": "String",
      "createdAt": "28-03-2018 09:56:27 AM",
      "updatedAt": "05-04-2018 01:09:47 PM",
      "activeFl": true
    },
    {
      "id": 2,
      "firstName": "Mithun",
      "lastName": "Mondal",
      "address": "String",
      "email": "String",
      "country": "String",
      "company": "Software",
      "status": "String",
      "createdAt": "05-04-2018 12:30:27 PM",
      "updatedAt": "05-04-2018 01:08:39 PM",
      "activeFl": false
    },
    {
      "id": 3,
      "firstName": "dimple",
      "lastName": "tiwari",
      "address": "MUMBAI",
      "email": "dimple@gmail.com",
      "country": "IND",
      "company": "LM",
      "status": "String",
      "createdAt": "05-04-2018 12:35:56 PM",
      "updatedAt": "05-04-2018 01:09:47 PM",
      "activeFl": false
    }
  ];

  public ageants;

  constructor(private _demoService: MyserviceService) { }
  ngOnInit() {
    // this.getAgents();

  }


}
