import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tutorland',
  templateUrl: './tutorland.component.html',
  styleUrls: ['./tutorland.component.css']
})
export class TutorlandComponent implements OnInit {

  counrty: any;
  usertype: any;
  username: any;
  detail1 = false;
  ff: boolean;
  ageants1: any;
  tutor1sa: Object[];
  public tutordata: any;
  f1: any;
  isDisabled51: string;
  isDisabled55: string;
  isDisabled5: string;
  isDisabled4: string;
  isDisabled3: string;
  tuorff: { "meta": { "code": number; "message": string; }; "data": { "id": number; "fullName": string; "residingAddress": string; "dob": string; "highestDegree": string; "majorMinor": string; "institutionOfDegree": string; "yearOfDegreeReceived": string; "registeringLocation": string; "tutorTPA": any[]; "tutorTEA": any[]; "tutorTPN": any[]; "tutorTPAS": any[]; "tutorTTA": any[]; "tutorTAE": any[]; "tutorTASE": any[]; "tutorTOE": any[]; "tutorTE": any[]; "tutorTCS": any[]; "tutorTKL": any[]; "tutorTA": any[]; "tutorTrainers": any[]; "tutorSubjects": any[]; "tutorOrders": any[]; "createdAt": string; "updatedAt": string; "activeFl": boolean; }[]; };
  tuorff1: any;

  isDisabled1: any;
  isDisabled: any;
  selectedValue1 = [];

  public tutor: any = {

  };

  public next = 1;
  public subject1: any = [];
  public aceept: any = [];
  public invoiceForm: FormGroup;
  datePipe = new DatePipe("en-US");
  selectedValue = [];
  public Fullname;
  public dob;
  public highestDegree;
  public majorMinor;
  public institutionOfDegree;
  public Address;
  public yearOfDegreeReceived;
  public registeringLocation;
  public Status;
  public previousaddress;
  public EmailAddress1;
  public previousaddress1;
  public tutor1;
  public dat;
  modalRef: BsModalRef;
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  dismissible = true;
  constructor(private _fb: FormBuilder, private _demoService: MyserviceService, private modalService: BsModalService, private router: Router) {
    if (sessionStorage.newtutor) {
      this.tutordata = JSON.parse(sessionStorage.newtutor);

      this.tutor.Firstname = this.tutordata.data[0].fullName;
      this.tutor.dob = this.tutordata.data[0].dob;

      this.tutor.dob = this.datePipe.transform(this.tutor.dob, 'MM/dd/yyyy');
      this.tutor.Residing = this.tutordata.data[0].residingAddress;
    }
  }

  ngOnInit() {


    this.invoiceForm = this._fb.group({
      tutorTPA: this._fb.array([this.initItemRows()]),
      tutorTEA: this._fb.array([this.initItemRows1()]),
      tutorTPN: this._fb.array([this.initItemRows2()]),
      tutorTPAS: this._fb.array([this.initItemRows3()]),
      tutorTTA: this._fb.array([this.initItemRows4()]),
      tutorTAE: this._fb.array([this.initItemRows5()]),
      tutorTASE: this._fb.array([this.initItemRows6()]),
      tutorTOE: this._fb.array([this.initItemRows7()]),
      tutorTE: this._fb.array([this.initItemRows8()])
    });

    this.getdata();
    this.username = sessionStorage.username;
    this.usertype = sessionStorage.usertype;


  }

  initItemRows() {
    return this._fb.group({
      address: ['']
    });
  }
  initItemRows1() {
    return this._fb.group({
      "email": ['', Validators.pattern],
      "typeOfAddress": ['1']

    });
  }
  initItemRows2() {
    return this._fb.group({
      "phoneNumber": [''],
      "typeOfNumber": ['1']

    });
  }

  initItemRows3() {
    return this._fb.group({
      "name": [''],
      "date": [''],
      "duration": ['']


    });
  }
  initItemRows4() {
    return this._fb.group({
      "date": [''],
      "location": [''],
      "accountNumber": ['']


    });


  }
  initItemRows5() {
    return this._fb.group({
      "date": [''],
      "location": ['']


    });


  }
  initItemRows6() {
    return this._fb.group({
      "name": [''],
      "url": ['']


    });
  }
  initItemRows7() {
    return this._fb.group({
      "name": [''],
      "url": ['']


    });
  }
  initItemRows8() {
    return this._fb.group({
      "name": [''],
      "locationOfSchool": [''],
      "gradeLvlOfStudent": [''],
      "subjectTaught": ['']


    });


  }

  signout() {
    this._demoService.signout();
  }

  showall() {
    if (this.ageants1) {
      this.detail1 = true;
    }
  }

  hideall() {
    this.detail1 = false;
  }

  getdata() {
    let url = "/plm/tutor?tutorId=" + this.tutordata.data[0].id;
    this._demoService.getfcountry(url).subscribe(
      data => {
        this.ageants1 = data;
        this.tutor.tutorTPA1 = this.ageants1.data[0].tutorTPA;
        this.tutor.tutorTEA1 = this.ageants1.data[0].tutorTEA;
        this.tutor.tutorTPN1 = this.ageants1.data[0].tutorTPN;
        if (this.ageants1.data[0].tutorTPAS.length != 0) {

          this.tutor.tutorTPAS1 = this.ageants1.data[0].tutorTPAS;
          this.tutor.ff = "true";
        }
        else {

          this.tutor.ff = "false";
          this.tutor.tutorTPAS1 = this.ageants1.data[0].tutorTPAS;

        }

        if (this.ageants1.data[0].tutorTTA.length != 0) {
          this.tutor.tutorTTA = this.ageants1.data[0].tutorTTA;
          this.tutor.ff1 = "true";
        }
        else {
          this.tutor.ff1 = "false";
          this.tutor.tutorTTA = this.ageants1.data[0].tutorTTA;
        }

        if (this.ageants1.data[0].tutorTAE.length != 0) {
          this.tutor.tutorTAE = this.ageants1.data[0].tutorTAE;
          this.tutor.f11 = "true";
        }
        else {
          this.tutor.f11 = "false";
          this.tutor.tutorTAE = this.ageants1.data[0].tutorTAE;
        }

        if (this.ageants1.data[0].tutorTASE.length != 0) {
          this.tutor.tutorTASE = this.ageants1.data[0].tutorTASE;
          this.tutor.f12 = "true";

        }
        else {
          this.tutor.f12 = "false";
          this.tutor.tutorTASE = this.ageants1.data[0].tutorTASE;
        }

        if (this.ageants1.data[0].tutorTOE.length != 0) {
          this.tutor.tutorTOE = this.ageants1.data[0].tutorTOE;
          this.tutor.isDisabled55 = "true";
        }
        else {
          this.tutor.isDisabled55 = "false";
          this.tutor.tutorTOE = this.ageants1.data[0].tutorTOE;
        }

        if (this.ageants1.data[0].tutorTE.length != 0) {
          this.tutor.tutorTE = this.ageants1.data[0].tutorTE;
          this.tutor.isDisabled51 = "true";

        }
        else {
          this.tutor.isDisabled51 = "false";
          this.tutor.tutorTE = this.ageants1.data[0].tutorTE;

        }

        this.tutor.highestDegree = this.ageants1.data[0].highestDegree;
        this.tutor.majorMinor = this.ageants1.data[0].majorMinor;
        this.tutor.remail = this.ageants1.data[0].plmUser.email;
        this.tutor.instname = this.ageants1.data[0].institutionOfDegree;
        this.tutor.yearOfDegreeReceived = this.ageants1.data[0].yearOfDegreeReceived;
        if (this.ageants1.data[0].tutorTCS) {

          for (let selecteds in this.ageants1.data[0].tutorTCS) {

            if (this.ageants1.data[0].tutorTCS[selecteds].section == "Basics") {
              this.tutor.Basics = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Writing") {
              this.tutor.Writing = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Reading") {
              this.tutor.Reading = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "English") {
              this.tutor.english = this.ageants1.data[0].tutorTCS[selecteds].section;
            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Math") {
              this.tutor.Math = this.ageants1.data[0].tutorTCS[selecteds].section;

            }
            else if (this.ageants1.data[0].tutorTCS[selecteds].section == "Science") {

              this.tutor.Science = this.ageants1.data[0].tutorTCS[selecteds].section;
            }

          }
          if ((this.tutor.Basics && this.tutor.Writing) && (this.tutor.Reading && this.tutor.english) && (this.tutor.Math && this.tutor.Science)) {
            this.tutor.Allsectionofact = "Allsectionofact";
          }
        }

        for (let selct in this.ageants1.data[0].tutorTKL) {

          if (this.ageants1.data[0].tutorTKL[selct].abilityType == "Speaking") {
            this.tutor.Speaking = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
          else if (this.ageants1.data[0].tutorTKL[selct].abilityType == "Writing") {
            this.tutor.Writing1 = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
          else if (this.ageants1.data[0].tutorTKL[selct].abilityType == "reading") {
            this.tutor.Reading1 = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }
          else if (this.ageants1.data[0].tutorTKL[selct].abilityType == "Verbal Comprehension") {
            this.tutor.Verbal = this.ageants1.data[0].tutorTKL[selct].proficiency.toString();
          }


        }

        this.tutor.loc = this.ageants1.data[0].registeringLocation;
        this.tutor.fcountry = this.ageants1.data[0].refCountryName;
        this.tutor.fcity = this.ageants1.data[0].refCityName;
        this.tutor.location1 = this.ageants1.data[0].futureRefLocation;
        this.counrty = this.ageants1.data[0].country.countryName;
        this.tutor.city = this.ageants1.data[0].registeringLocation.split(" ")[0];
        if (this.ageants1.data[0].tutorTrainers.length != 0) {
          this.tutor.Trainers = this.ageants1.data[0].tutorTrainers[0].trainerName;
          this.tutor.Address = this.ageants1.data[0].tutorTrainers[0].trainerAddress;
        }
        this.tutor.tutorapp1 = this.ageants1.data[0].tutorTA[1].declareType;
        this.tutor.tutorapp2 = this.ageants1.data[0].tutorTA[0].declareType;
        this.tutor.tutorapp3 = this.ageants1.data[0].tutorTA[2].declareType;
        this.tutor.tutorapp4 = this.ageants1.data[0].tutorTA[3].declareType;
        this.tutor.tutorapp5 = this.ageants1.data[0].tutorTA[4].declareType;
        this.tutor.tutorapp6 = this.ageants1.data[0].tutorTA[5].declareType;
        this.tutor.tutorapp7 = this.ageants1.data[0].tutorTA[6].declareType;
        this.tutor.tutorapp8 = this.ageants1.data[0].tutorTA[7].declareType;
        this.tutor.tutorapp9 = this.ageants1.data[0].tutorTA[8].declareType;
        this.tutor.tutorapp10 = this.ageants1.data[0].tutorTA[9].declareType;
        this.tutor.tutorapp11 = this.ageants1.data[0].tutorTA[10].declareType;
        this.tutor.tutorapp12 = this.ageants1.data[0].tutorTA[11].declareType;

        this.tutor.Start = this.datePipe.transform(this.ageants1.data[0].futureRefStartDate, 'MM-DD-YYYY');
        this.tutor.End = this.datePipe.transform(this.ageants1.data[0].futureRefEndDate, 'MM-DD-YYYY');

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));

      },

    )
  }

  goto(i) {
    this.next = i;

  }

  nextt() {

    if (this.next < 12) {
      this.next = this.next + 1;
    }
  }

  backt() {
    if (this.next > 1)
      this.next = this.next - 1;
  }

}
