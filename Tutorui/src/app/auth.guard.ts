import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
     if(sessionStorage.data !="d4etmfBQ-EsarzuoGSdCVm981IVn4ZS8ZPfYfmJNuRcIDLrKdct_U_NAxqSnQMh0MSh0OZcJ4hxbKiy89_KV0isKLQsH0BbP32AWd5gAB7k" && sessionStorage.data){
     
      return true;
  }
 else{
   
    this._router.navigate(['/login']);
    return false; 
 }
}
}
