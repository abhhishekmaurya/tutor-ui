import { Component, OnInit, TemplateRef } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-testdate',
  templateUrl: './testdate.component.html',
  styleUrls: ['./testdate.component.css']
})

export class TestdateComponent implements OnInit {
  company: string;
  country: string;
  // dtOptions: any = {};
  public temp_var: Object = false;

  Testdateuser: any;
  Testdateuser1: any;
  public Testdate1;
  datePipe = new DatePipe("en-US");

  public food1;
  public value;
  modalRef: BsModalRef;
  public testName;
  public address;
  public time;
  public subject;
  public Company;
  public Address;
  public location;
  public Country;
  public Status;
  public trainingStartDate;
  public trainingEndDate;
  test1 = true;
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  alerts1: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  dismissible = true;

  dtOptions = {
    paging: true,
    pageLength: 10,
    processing: true,
    columnDefs: [ {

      targets: [0,6], // column or columns numbers
      
      orderable: false,  // set orderable for selected columns
      
      }]

  };

  constructor(private _demoService: MyserviceService, private modalService: BsModalService, private router: Router) {



    this.getTestdate();


  }

  ngOnInit() {
    // this.dtOptions = {
    //   paging: true,
    //   pageLength: 10,
    //   processing: true,
    //   columnDefs: [ {

    //     targets: [0], // column or columns numbers
        
    //     orderable: false,  // set orderable for selected columns
        
    //     }],

    // };
  }

  username = this._demoService.username;
  openModal(template: TemplateRef<any>, f) {
    sessionStorage.models = 'true';
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.food1 = f;
    this.food1.firstName = f.firstName;
    this.modalRef = this.modalService.show(template);

  }

  openModal1(template: TemplateRef<any>) {
    sessionStorage.models = 'true';
    this.cleardata();
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.modalRef = this.modalService.show(template);

  }

  openModalWithComponent(template: TemplateRef<any>, f) {
    sessionStorage.models = 'true';

    this.food1 = f;
    this.food1.trainingStartDate = new Date(f.trainingStartDate);

    this.food1.trainingEndDate = new Date(f.trainingEndDate);
    this.alerts1 = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this.food1.firstName = f.firstName;
    this.modalRef = this.modalService.show(template);
  }

  cleardata() {

    this.testName = "";
    this.time = "";
    this.subject = "";
    this.trainingEndDate = "";
    this.trainingStartDate = "";
    this.location = "";
    this.country = "";
    this.address = "";
    this.company = "";


  }
  
  getTestdate() {
    let url = "/plm/testDates?filter=%7B%7D";
    this._demoService.getfcountry(url).subscribe(
      data => {
        this.Testdateuser = data;
        this.Testdateuser = this.Testdateuser.data;
        this.temp_var = true;

      },
      error => {

        alert(JSON.stringify(error.error.meta.error_message));

        return Observable.throw(error);
      }

    )
  }

  addTestdata(testName1, time1, subject1, company1, address1, country1, trainingStartDate1, trainingEndDate1) {

    this.value = this.datePipe.transform(trainingStartDate1, 'yyyy-MM-dd');
    var value = this.value;
    trainingStartDate1 = value;

    this.value = this.datePipe.transform(trainingEndDate1, 'yyyy-MM-dd');
    var value1 = this.value;
    trainingEndDate1 = value1;

    if (!testName1) {

      this.alerts1.push({
        type: 'danger',
        msg: `TestName is required !!!`,
        timeout: 5000
      });
    }
    else if (!time1) {

      this.alerts1.push({
        type: 'danger',
        msg: `Time is required !!!`,
        timeout: 5000
      });
    }
    else if (!subject1) {

      this.alerts1.push({
        type: 'danger',
        msg: `Subject name is required !!!`,
        timeout: 5000
      });

    }
    else if (!company1) {
      this.alerts1.push({
        type: 'danger',
        msg: `Company name is required !!!`,
        timeout: 5000
      });
    }
    else if (!address1) {
      this.alerts1.push({
        type: 'danger',
        msg: `Address is required !!!`,
        timeout: 5000
      });
    } else if (!country1) {
      this.alerts1.push({
        type: 'danger',
        msg: `Country name is required !!!`,
        timeout: 5000
      });
    }
    else if (!trainingStartDate1) {

      this.alerts1.push({
        type: 'danger',
        msg: `Traning Start date is required !!!`,
        timeout: 5000
      });
    }
    else if (!trainingEndDate1) {

      this.alerts1.push({
        type: 'danger',
        msg: `Traning end date is required!!!`,
        timeout: 5000
      });
    }
    else if (trainingStartDate1 > trainingEndDate1) {
      this.alerts1.push({
        type: 'danger',
        msg: `End Date cannot be less than the start date !!!`,
        timeout: 5000
      });
    }
    else {
      let addnewTest = {

        "testName": testName1,
        "address": address1,
        "time": time1,
        "location": address1,
        "country": country1,
        "company": company1,
        "subject": subject1,
        "trainingStartDate": trainingStartDate1,
        "trainingEndDate": trainingEndDate1,
        "status": 'Active'
      };

      let url = "/plm/testDate?userId=";
      this._demoService.postdata(url, addnewTest).subscribe(
        data => {
          this.Testdateuser = data;



          this.getTestdate();
          this.cleardata();
          this.alerts.push({
            type: 'success',
            msg: `Test Data Created Sucessfully !!!`,
            timeout: 5000
          });

        },
        error => {

          alert(JSON.stringify(error.error.meta.error_message));
          return Observable.throw(error);
        }

      );

      this.modalRef.hide();
    }

  }


  updateTestdata(s) {

    if (!s.testName) {

      this.alerts1.push({
        type: 'danger',
        msg: `TestName is required !!!`,
        timeout: 5000
      });
    }
    else if (!s.time) {

      this.alerts1.push({
        type: 'danger',
        msg: `Time is required !!!`,
        timeout: 5000
      });
    }
    else if (!s.subject) {

      this.alerts1.push({
        type: 'danger',
        msg: `Subject name is required !!!`,
        timeout: 5000
      });
    }
    else if (!s.company) {

      this.alerts1.push({
        type: 'danger',
        msg: `Company name  is required !!!`,
        timeout: 5000
      });
    }
    else if (!s.address) {

      this.alerts1.push({
        type: 'danger',
        msg: `Address  is required !!!`,
        timeout: 5000
      });
    } else if (!s.country) {

      this.alerts1.push({
        type: 'danger',
        msg: `Country name  is required !!!`,
        timeout: 5000
      });
    }
    else if (!s.trainingStartDate) {

      this.alerts1.push({
        type: 'danger',
        msg: `Traning Start date  is required !!!`,
        timeout: 5000
      });
    }
    else if (!s.trainingEndDate) {

      this.alerts1.push({
        type: 'danger',
        msg: `Traning end date  is required !!!`,
        timeout: 5000
      });
    }
    if (s.trainingStartDate > s.trainingEndDate) {
      this.alerts1.push({
        type: 'danger',
        msg: `End date should be greater than Start date!!!`,
        timeout: 5000
      });
    }
    else {

      let url = "/plm/testDate";
      this._demoService.edits(url, s).subscribe(
        data => {
          this.Testdateuser = data;
          this.getTestdate();

          this.alerts.push({
            type: 'success',
            msg: `Test Data updated Sucessfully !!!`,
            timeout: 5000
          });
        },
        error => {

          alert(JSON.stringify(error.error.meta.error_message));
          return Observable.throw(error);
        }

      );
      this.modalRef.hide();
    }
  }

  deleteTestdate(f) {
    this.alerts.push({
      type: '',
      msg: ``,
      timeout: 5000
    });
    if (confirm("Are you sure you want to delete " + "?")) {
      let url = '/plm/testDate?ids=' + f.id;
      this._demoService.delete(url).subscribe(
        data => {
           this.Testdateuser = data;
           this.getTestdate();
           this.alerts.push({
            type: 'success',
            msg: this.Testdateuser.data[0],
            timeout: 5000
          });

        },
        error => {
          alert(JSON.stringify(error.error.meta.error_message));

        }


      )


    }
  }

  checkAll(ev) {

    this.Testdateuser.forEach(x => x.state = ev.target.checked);

  }

  isAllChecked() {


    return this.Testdateuser.every(_ => _.state);
  }

  ngOnDestroy() {
    if (sessionStorage.models == 'true') {
      this.modalRef.hide();
      sessionStorage.models = false;
    }

  }





}
