import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TestdateComponent } from './testdate.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HeadervComponent } from '../headerv/headerv.component';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule, ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';

describe('TestdateComponent', () => {

  let component: TestdateComponent;
  let fixture: ComponentFixture<TestdateComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({

      imports: [RouterTestingModule, HttpClientModule, AlertModule.forRoot(), FormsModule, ModalModule.forRoot(), BsDatepickerModule.forRoot(), DataTablesModule],
      declarations: [TestdateComponent, HeadervComponent],
      providers: [MyserviceService, AuthGuard]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
