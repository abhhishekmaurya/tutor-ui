import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupComponent } from './signup.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule, ModalModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { MyserviceService } from '../services/myservice.service';
import { AuthGuard } from '../auth.guard';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientModule,AlertModule.forRoot(),FormsModule,ModalModule.forRoot() ], 
      declarations: [ SignupComponent ],
      providers : [MyserviceService,AuthGuard]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
