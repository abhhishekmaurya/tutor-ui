import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  tutor1: any;
  firstname: any;
  lastname: any;
  email1: any;
  pass1: any;
  pass2: any;
  dismissible = true;
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  constructor(private _demoService: MyserviceService, private router: Router) { }

  ngOnInit() {
  }

  createUser(firstname, lastname, email1, pass1, pass2) {

    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];

    if (!firstname) {

      this.alerts.push({
        type: 'danger',
        msg: `Firstname is required !!!`,
        timeout: 5000
      });
    }
    else if (!lastname) {
      this.alerts.push({
        type: 'danger',
        msg: `Lastname is required !!!`,
        timeout: 5000
      });
    }
    else if (!email1) {

      this.alerts.push({
        type: 'danger',
        msg: `EmailID is required !!!`,
        timeout: 5000
      });
    }
    else if (!pass1) {

      this.alerts.push({
        type: 'danger',
        msg: `Password is required !!!`,
        timeout: 5000
      });
    }
    else if (!pass2) {

      this.alerts.push({
        type: 'danger',
        msg: `Please enter confirm password !!!`,
        timeout: 5000
      });
    }
    else if (pass1 != pass2) {

      this.alerts.push({
        type: 'danger',
        msg: `Password and Confirm password should be same !!!`,
        timeout: 5000
      });

    }
    else {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(String(email1))) {
        let body = {
          "firstName": firstname, "lastName": lastname, "email": email1, "userPassword": pass1
        };
        this._demoService.createuser(body).subscribe(
          data => {
            this.tutor1 = data;

            alert("User Added Sucessfully!");

            this._demoService.token = this.tutor1.data[0].ecAuthToken;
            sessionStorage.data = this.tutor1.data[0].ecAuthToken;
            sessionStorage.username = this.tutor1.data[0].firstName;
            sessionStorage.usertype = this.tutor1.data[0].roleType;
            sessionStorage.uid = this.tutor1.data[0].id;
            sessionStorage.fname = this.tutor1.data[0].firstName;
            sessionStorage.lname = this.tutor1.data[0].lastName;
            sessionStorage.emails = this.tutor1.data[0].email;

            this.router.navigate(['/Newtutor']);

          },
          error => {

            this.alerts.push({
              type: 'danger',
              msg: error.error.meta.error_message,
              timeout: 5000
            });


          }


        );

      }
      else {
        this.alerts.push({
          type: 'danger',
          msg: `Invalid Emailid !!!`,
          timeout: 5000
        });


      }


    }

  }
}
