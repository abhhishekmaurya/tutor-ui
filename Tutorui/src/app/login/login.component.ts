import { Component, OnInit, TemplateRef } from '@angular/core';
import { MyserviceService } from '../services/myservice.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  pass2: string;
  pass1: string;
  public pass: any;
  username1: string;
  tutor2: Object;
  tutor1: any;
  username;
  dismissible = true;
  alerts: any[] = [{
    type: '',
    msg: ``,
    timeout: 5000
  }];
  modalRef: BsModalRef;
  constructor(private _demoService: MyserviceService, private router: Router, private modalService: BsModalService) { }

  ngOnInit() {

  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  checkusertutor(bid) {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    this._demoService.loginuserrole(bid).subscribe(
      data => {
        this.tutor2 = data;
        sessionStorage.newtutor = JSON.stringify(this.tutor2);
        this.router.navigate(['/tutorwel']);

      },
      error => {

        this.alerts.push({
          type: 'danger',
          msg: error.error.meta.error_message,
          timeout: 5000
        });

        if (error.error.meta.code == 404) {
          this.router.navigate(['/Newtutor']);

        }
        else {
          this.alerts.push({
            type: 'danger',
            msg: error.error.meta.error_message,
            timeout: 5000
          });
        }

      }
    );

  }


  loginuser(s, d) {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    if (!s) {

      this.alerts.push({
        type: 'danger',
        msg: `Email is required !!!`,
        timeout: 5000
      });
    }
    else if (!d) {

      this.alerts.push({
        type: 'danger',
        msg: `Password is required !!!`,
        timeout: 5000
      });
    }
    else {
      let body = {
        "userPassword": d,
        "email": s
      }

      this._demoService.loginuser(body).subscribe(
        data => {
          this.tutor1 = data;
          this._demoService.username = this.tutor1.data[0].fullName;
          this._demoService.token = this.tutor1.data[0].ecAuthToken;
          sessionStorage.data = this.tutor1.data[0].ecAuthToken;
          sessionStorage.username = this.tutor1.data[0].firstName;
          sessionStorage.usertype = this.tutor1.data[0].roleType;
          sessionStorage.uid = this.tutor1.data[0].id;
          sessionStorage.fname = this.tutor1.data[0].firstName;
          sessionStorage.lname = this.tutor1.data[0].lastName;
          sessionStorage.emails = this.tutor1.data[0].email;

          // ADMIN
          if (sessionStorage.usertype != 'ADMIN') {


            this.checkusertutor(body);
          }
          else {
            this.router.navigate(['/home']);
          }
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.error.meta.error_message,
            timeout: 5000
          });

        }

      )
    }
  }

  forgetpass(template: TemplateRef<any>) {
    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];

    this.modalRef = this.modalService.show(template);
  }

  resetpass(n, e, s) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    this.alerts = [{
      type: '',
      msg: ``,
      timeout: 5000
    }];
    s = "";
    if (!n) {
      this.alerts = [{
        type: 'danger',
        msg: 'Please Enter Email ID.',
        timeout: 5000
      }];
    }

    else {
      if (re.test(String(n))) {
        let body = {
          "userPassword": s,
          "email": n
        }

        this._demoService.forgetpass(body).subscribe(
          data => {
            this.tutor1 = data;
            this.alerts.push({
              type: 'success',
              msg: 'Mail sent successfully.',
              timeout: 5000
            });
            alert("Mail sent successfully.");
            this.username1 = "";
            this.pass1 = "";
            this.pass2 = "";
            this.modalRef.hide();

          },

          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.error.meta.error_message,
              timeout: 5000
            });
          }

        );
      }

      else {

        this.alerts.push({
          type: 'danger',
          msg: `Invalid Emailid !!!`,
          timeout: 5000
        });
      }
    }
  }
}
